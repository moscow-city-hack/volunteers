import React from 'react'
import { Provider } from 'react-redux'
import { Route, Routes, BrowserRouter } from 'react-router-dom'
import { QueryClient, QueryClientProvider } from 'react-query'
import createMuiTheme from '@material-ui/core/styles/createMuiTheme'
import MuiThemeProvider from '@material-ui/core/styles/MuiThemeProvider'
import { SnackbarProvider } from 'notistack'
import { SnackbarUtilsConfigurator } from 'config/snackbars/snackbarConfig'
import Home from 'pages/Home/Home'
import Volunteers from 'pages/Volunteers/Volunteers'
import Organisations from 'pages/Organisations/Organisations'
import GoodWorks from 'pages/GoodWorks/GoodWorks'
import Login from 'pages/Login/Login'
import Registration from 'pages/Registration/Registration'
import PersonalCabinet from 'pages/PersonalCabinet/PersonalCabinet'
import './styles/index.scss'

export const muiTheme = createMuiTheme({
  typography: {
    fontFamily: ['Noto Sans', 'sans-serif'].join(','),
    useNextVariants: true,
  },
})

const queryClient = new QueryClient({
  defaultOptions: {
    queries: {
      retry: process.env.NODE_ENV === 'production',
      refetchOnWindowFocus: process.env.NODE_ENV === 'production',
    },
  },
})

export const snackBarClasses = {
  variantSuccess: 'snackbar--success',
  variantError: 'snackbar--error',
  variantWarning: 'snackbar--warning',
  variantInfo: 'snackbar--info',
}

const App = ({ store, history }) => (
  <QueryClientProvider client={queryClient}>
    <MuiThemeProvider theme={muiTheme}>
      <ConnectedSnackbarProvider>
        <Provider store={store}>
          <BrowserRouter history={history}>
            <SnackbarUtilsConfigurator />
            <Routes>
              <Route path="/" element={<Home />}></Route>
              <Route path="/volunteers" element={<Volunteers />}></Route>
              <Route path="/organisations" element={<Organisations />}></Route>
              <Route path="/events" element={<GoodWorks />}></Route>
              <Route path="/login" element={<Login />}></Route>
              <Route path="/register" element={<Registration />}></Route>
              <Route path="/personal" element={<PersonalCabinet />}></Route>
            </Routes>
          </BrowserRouter>
        </Provider>
      </ConnectedSnackbarProvider>
    </MuiThemeProvider>
  </QueryClientProvider>
)

const ConnectedSnackbarProvider = ({ children }) => (
  <SnackbarProvider anchorOrigin="bottom-right" classes={snackBarClasses}>
    {children}
  </SnackbarProvider>
)

export default App
