import { ReactComponent as ChevronDown } from './chevron-down.svg'
import { ReactComponent as ChevronRight } from './chevron-right.svg'
import { ReactComponent as Calendar } from './calendar.svg'
import { ReactComponent as Close } from './close.svg'
import { ReactComponent as EyeOpened } from './eye-opened.svg'
import { ReactComponent as EyeClosed } from './eye-closed.svg'
import { ReactComponent as Time } from './time.svg'
import { ReactComponent as Minus } from './minus.svg'
import { ReactComponent as Checked } from './checked.svg'
import { ReactComponent as Search } from './search.svg'
import { ReactComponent as Heart } from './heart.svg'
import { ReactComponent as HeartFilled } from './heart-filled.svg'
import { ReactComponent as Location } from './location.svg'
import { ReactComponent as Star } from './star.svg'
import { ReactComponent as StarFilled } from './star-filled.svg'
import { ReactComponent as Chat } from './chat.svg'
import { ReactComponent as ArrowUpRight } from './arrow-up-right.svg'
import { ReactComponent as Animals } from './animals.svg'
import { ReactComponent as Nature } from './nature.svg'
import { ReactComponent as Education } from './education.svg'
import { ReactComponent as Map } from './map.svg'
import { ReactComponent as List } from './list.svg'
import { ReactComponent as User } from './user.svg'
import { ReactComponent as Logout } from './logout.svg'
import { ReactComponent as Home } from './home.svg'
import { ReactComponent as Doc } from './doc.svg'

export const ICONS = {
  chevronDown: ChevronDown,
  chevronRight: ChevronRight,
  calendar: Calendar,
  close: Close,
  eyeOpened: EyeOpened,
  eyeClosed: EyeClosed,
  time: Time,
  minus: Minus,
  checked: Checked,
  search: Search,
  heart: Heart,
  heartFilled: HeartFilled,
  location: Location,
  star: Star,
  starFilled: StarFilled,
  chat: Chat,
  arrowUpRight: ArrowUpRight,
  animals: Animals,
  nature: Nature,
  education: Education,
  map: Map,
  list: List,
  user: User,
  logout: Logout,
  home: Home,
  doc: Doc,
}
