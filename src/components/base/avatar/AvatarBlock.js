import React from 'react'
import size from 'lodash/size'
import Row from 'components/base/row/Row'
import Text from 'components/base/text/Text'
import Rating from 'components/base/rating/Rating'
import './avatar-block.scss'

export type AvatarBlockType = {
  image?: string,
  name?: string,
  id?: string,
  rating?: number,
  reviews?: number,
}

export type AvatarBlockProps = AvatarBlockType & {}

const AvatarBlock = (props: AvatarBlockProps) => {
  const { image = '', name = '', id = '', rating, reviews = 0 } = props

  const hasImage = size(image) > 0
  const hasName = size(name) > 0
  const hasId = Boolean(id)

  const hasRating = Boolean(rating)
  const hasReviews = reviews > 0
  const hasMark = hasRating && hasReviews

  const hasContent = hasImage || hasName || hasId || hasMark

  return hasContent ? (
    <Row className="avatar-block">
      {hasImage && <img className="avatar-block__image" src={image} alt="" />}
      <div className="avatar-block__content">
        {hasName && (
          <Text type="semibold" variant="sm" className="avatar-block__name">
            {name}
          </Text>
        )}
        {hasId && <Text variant="sm">{['ID', id].join(' ')}</Text>}
        {hasMark && (
          <Row>
            <Rating value={rating} type="small" />
            {hasReviews && (
              <Text variant="sm" className="avatar-block__reviews">{`(${reviews})`}</Text>
            )}
          </Row>
        )}
      </div>
    </Row>
  ) : null
}

export default AvatarBlock
