import React from 'react'
import Text from 'components/base/text/Text'

export type AboutBadgeType = {
  count: number,
  text: string,
}

export type AboutInfoBadgeProps = AboutBadgeType

const AboutInfoBadge = (props: AboutInfoBadgeProps) => {
  const { text, count } = props

  return (
    <div className="about__info-item">
      {count > 0 && <Text type="semibold">{count}</Text>}
      <Text variant="sm">{text}</Text>
    </div>
  )
}

export default AboutInfoBadge
