import React, { ReactNode } from 'react'
import lodashSize from 'lodash/size'
import classNames from 'classnames'
import Row from 'components/base/row/Row'
import Text from 'components/base/text/Text'
import './badge.scss'

export type BadgeItemType = {
  text: string,
  minutes: number,
  background: string,
}

export type BadgeProps = Partial<BadgeItemType> & {
  color?: 'base' | 'white',
  size?: 'base' | 'small',
  renderAtEnd?: () => ReactNode,
  textProps?: Record<string, any>,
}

const Badge = (props: BadgeProps) => {
  const {
    text = '',
    background = '#FFFFFF',
    color = 'base',
    size = 'base',
    renderAtEnd,
    textProps = {},
  } = props

  const hasText = lodashSize(text) > 0
  const hasContent = Boolean(renderAtEnd) || hasText

  const textSettings = {
    base: { variant: 'md', type: 'medium' },
    small: { variant: 'sm' },
  }

  return hasContent ? (
    <Row
      horizontal="between"
      style={{ background }}
      className={classNames('badge', {
        [size]: size !== 'base',
        [color]: color !== 'base',
      })}
    >
      {hasText && (
        <Text {...textProps} {...textSettings[size]}>
          {text}
        </Text>
      )}
      {renderAtEnd?.()}
    </Row>
  ) : null
}

export default Badge
