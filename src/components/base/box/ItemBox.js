import React, { useCallback } from 'react'
import classNames from 'classnames'
import isNil from 'lodash/isNil'
import Text from 'components/base/text/Text'
import SvgIcon from 'components/base/icon/SvgIcon'
import { ICONS } from 'assets/icons/icons'
import { stopPropagationEvent } from 'utils/common'
import './box.scss'

export type ItemBoxProps = {
  className?: string,
  item: any,
  onClickItem?: (item: any, index: number) => void,
  onClickRemove?: (item: any, index: number, event: any) => void,
  defaultTitle?: string,
  index?: number,
  activeItemIndex?: number,
  active?: boolean,
  disabled?: boolean,
  invalid?: boolean,
}

const ItemBox = (props: ItemBoxProps) => {
  const {
    className = '',
    item,
    index,
    activeItemIndex,
    onClickItem,
    onClickRemove,
    defaultTitle = '',
    active = false,
    disabled = false,
    invalid = false,
    ...other
  } = props

  const _onClickItem = useCallback(
    (event: MouseEvent<HTMLElement>) => {
      stopPropagationEvent(event)
      onClickItem?.(item, index)
    },
    [onClickItem, item, index],
  )

  const _onClickRemove = useCallback(
    (event: MouseEvent<HTMLElement>) => {
      stopPropagationEvent(event)
      onClickRemove(item, index, event)
    },
    [onClickRemove, item, index],
  )

  return (
    <div
      {...other}
      className={classNames([
        'item-box',
        className,
        {
          'item-box--active':
            active || (!isNil(activeItemIndex) && activeItemIndex === index),
          'item-box--disabled': disabled,
          'item-box--invalid': invalid,
          'item-box--clickable': Boolean(onClickItem),
        },
      ])}
      onClick={_onClickItem}
    >
      {item.hasIcon && (
        <SvgIcon Icon={ICONS[item.id]} className="item-box__icon" />
      )}
      <Text className="item-box__text">{item.displayname || defaultTitle}</Text>
      {Boolean(onClickRemove) && !disabled && (
        <SvgIcon
          Icon={ICONS.close}
          className="item-box__remover"
          onClick={_onClickRemove}
        />
      )}
    </div>
  )
}

export default ItemBox
