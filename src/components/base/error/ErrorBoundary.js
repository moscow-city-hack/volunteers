import React, { Component, ErrorInfo } from 'react'
import Text from 'components/base/text/Text'

/**
 * @see https://ru.reactjs.org/docs/error-boundaries.html
 */
export default class ErrorBoundary extends Component {
  state = { error: null }

  static getDerivedStateFromError(error) {
    return { error }
  }

  componentDidCatch(error: Error, errorInfo: ErrorInfo) {
    console.error('Возникла ошибка отрисовки', error, errorInfo)
  }

  render() {
    if (this.state.error) {
      return <Text variant="md">{this.props.message || this.state.error?.toString()}</Text>
    }

    return this.props.children
  }
}
