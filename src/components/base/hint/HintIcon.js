import React, { CSSProperties, ReactComponentElement } from 'react'
import classNames from 'classnames'
import Tooltip from 'components/base/tooltip/Tooltip'
import './hint.scss'

export type HintIconProps = {
  className?: string,
  Component?: ReactComponentElement,
  title?: string,
  onClick?: (event: MouseEvent) => void,
  isInline?: boolean,
  backgroundColor?: string,
  style?: CSSProperties,
}

const HintIcon = (props: HintIconProps) => {
  const {
    className = '',
    Component = 'div',
    title,
    onClick,
    isInline = false,
    backgroundColor = false,
    style = {},
  } = props

  return (
    <Tooltip title={title}>
      <Component
        className={classNames([
          className,
          'hint-icon',
          {
            'hint-icon--disabled': !title && !onClick,
            'hint-icon--inline': isInline,
            'hint-icon--clickable': !!onClick,
          },
        ])}
        onClick={onClick}
        style={{ backgroundColor, ...style }}
      >
        {'?'}
      </Component>
    </Tooltip>
  )
}

export default HintIcon
