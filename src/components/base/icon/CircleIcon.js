import React, { MouseEvent, useCallback, forwardRef, Ref } from 'react'
import omit from 'lodash/omit'
import classNames from 'classnames'
import SvgIcon from 'components/base/icon/SvgIcon'
import { stopPropagationEvent } from 'utils/common'
import type { SvgIconProps } from 'components/base/icon/SvgIcon'

export type CircleIconProps = {
  size?: 'base' | 'small',
  circleClassName?: string,
} & SvgIconProps

const CircleIcon = (props: CircleIconProps, ref: Ref) => {
  const { onClick, Icon, isClickable, size = 'base', circleClassName = '' } = props

  // Перехватываем клик у SvgIcon
  const _onClick = useCallback(
    (event: MouseEvent<HTMLDivElement>) => {
      if (onClick) {
        stopPropagationEvent(event)
        onClick()
      }
    },
    [onClick],
  )

  return (
    <>
      {Boolean(Icon) && (
        <div
          className={classNames('circle-icon', circleClassName, {
            'circle-icon--clickable': isClickable || Boolean(onClick),
            [size]: size !== 'base',
          })}
          onClick={_onClick}
          ref={ref}
        >
          <SvgIcon {...omit(props, 'onClick', 'circleClassName')} />
        </div>
      )}
    </>
  )
}

export default forwardRef(CircleIcon)
