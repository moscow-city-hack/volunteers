import React, { ReactNode } from 'react'
import classNames from 'classnames'
import Row from 'components/base/row/Row'
import SvgIcon from 'components/base/icon/SvgIcon'
import Text from 'components/base/text/Text'
import { ICONS } from 'assets/icons/icons'
import './info.scss'

export type InfoType = {
  id: string,
  text: string,
}

export type InfoProps = InfoType & {
  Component?: ReactNode,
  className?: string,
}

const Info = (props: InfoProps) => {
  const { id = '', text = '', Component, className = '', ...other } = props

  return (
    <Row Component={Component} className={classNames('info', className)} {...other}>
      <SvgIcon Icon={ICONS[id]} className="info__icon" />
      <Text type="medium" className="info__text">
        {text}
      </Text>
    </Row>
  )
}

export default Info
