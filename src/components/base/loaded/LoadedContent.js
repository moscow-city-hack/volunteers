import React from 'react'
import size from 'lodash/size'
import Preloader from 'components/base/preloader/Preloader'

export type LoadedContentProps = {
  isLoading: boolean,
  data: Record<string, any>,
  renderContent: () => ReactNode,
}

const LoadedContent = (props: LoadedContentProps) => {
  const { isLoading = true, data = {}, renderContent } = props

  return isLoading ? <Preloader size={80} /> : size(data) > 0 ? <>{renderContent?.()}</> : null
}

export default LoadedContent
