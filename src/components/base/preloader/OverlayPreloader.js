import React from 'react'
import classNames from 'classnames'
import Preloader from './Preloader'
import './preloader.scss'

export type OverlayPreloaderProps = {
  className?: string,
  showSpinner?: boolean,
  backgroundAlpha?: number,
}

const OverlayPreloader = (props: OverlayPreloaderProps) => {
  const { className = '', showSpinner = false, backgroundAlpha = 0.6, ...other } = props

  return (
    <div
      {...other}
      style={{ opacity: backgroundAlpha }}
      className={classNames('overlay-preloader', className)}
    >
      {showSpinner ? <Preloader /> : null}
    </div>
  )
}

export default OverlayPreloader
