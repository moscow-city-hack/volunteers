import React from 'react'
import classNames from 'classnames'
import './preloader.scss'

export type PreloaderProps = {
  className?: string,
  isStatic?: boolean,
  size?: number,
}

const Preloader = (props: PreloaderProps) => {
  const { className = '', isStatic = false, size = 24 } = props

  return (
    <div
      className={classNames([
        'preloader',
        `preloader--${isStatic ? 'static' : 'absolute'}`,
        className,
      ])}
      style={{ width: size, height: size }}
    >
      {Array.from(Array(10).keys()).map((_, index) => (
        <div key={index + 1} />
      ))}
    </div>
  )
}

export default Preloader
