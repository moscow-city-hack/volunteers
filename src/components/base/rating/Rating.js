import React from 'react'
import classNames from 'classnames'
import Row from 'components/base/row/Row'
import SvgIcon from 'components/base/icon/SvgIcon'
import { ICONS } from 'assets/icons/icons'
import './rating.scss'

export type RatingProps = {
  value: number,
  type?: 'base' | 'small',
}

const Rating = (props: RatingProps) => {
  const { value = 0, type = 'base' } = props

  return (
    <Row
      horizontal="between"
      className={classNames('rating', { [type]: type !== 'base' })}
    >
      {[...Array(5)].map((_, index) => (
        <SvgIcon key={index} Icon={value > index ? ICONS.starFilled : ICONS.star} />
      ))}
    </Row>
  )
}

export default Rating
