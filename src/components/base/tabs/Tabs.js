import React, { useState, useCallback } from 'react'
import classNames from 'classnames'
import Row from 'components/base/row/Row'
import SvgIcon from 'components/base/icon/SvgIcon'
import BaseCard from 'components/cards/base/BaseCard'
import Text from 'components/base/text/Text'
import './tabs.scss'

export type TabsProps = {
  activeTab?: number | string,
  tabs?: Array<Record<string, any>>,
  renderContent: Record<string, any>,
}

const Tabs = (props: TabsProps) => {
  const { activeTab, tabs = [], renderContent = {} } = props

  const [selected, setSelected] = useState(activeTab ?? tabs[0]?.id)

  const onClickTab = useCallback((id) => setSelected(id), [setSelected])

  return (
    <Row className="tabs">
      <BaseCard
        className="tabs__side"
        renderContent={() => (
          <div className="tabs__items">
            {tabs.map(({ text, id, icon }) => {
              return (
                <Row
                  key={id}
                  className={classNames('tabs__item', { active: id === selected })}
                  onClick={() => onClickTab(id)}
                >
                  <SvgIcon Icon={icon} className="tabs__item-icon" />
                  <Text type="semibold" variant="sm" className="tabs__item-text">
                    {text}
                  </Text>
                </Row>
              )
            })}
          </div>
        )}
      />
      <div className="tabs__inner">
        {
          <Text type="semibold" variant="xl" className="tabs__title">
            {tabs?.find(({ id }) => id === selected)?.text ?? ''}
          </Text>
        }
        <div className="tabs__content">{renderContent[selected]}</div>
      </div>
    </Row>
  )
}

export default Tabs
