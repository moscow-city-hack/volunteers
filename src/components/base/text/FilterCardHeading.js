import React from 'react'
import size from 'lodash/size'
import Text from 'components/base/text/Text'

export type FilterCardHeadingProps = {
  text?: string,
}

const FilterCardHeading = (props: FilterCardHeadingProps) => {
  const { text = '' } = props

  return size(text) > 0 ? <Text className="filter__name">{text}</Text> : null
}

export default FilterCardHeading
