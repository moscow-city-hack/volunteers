import React from 'react'
import lodashSize from 'lodash/size'
import classNames from 'classnames'
import Text from 'components/base/text/Text'

export type HeadingProps = {
  text: string,
  withSpacing?: boolean,
  size?: 'base' | 'small',
}

const Heading = (props: HeadingProps) => {
  const { text = '', withSpacing = true, size = 'base' } = props

  return lodashSize(text) > 0 ? (
    <Text
      type="semibold"
      variant={size === 'base' ? 'xxl' : 'lg'}
      className={classNames('heading', { 'heading--spacing': withSpacing })}
    >
      {text}
    </Text>
  ) : null
}

export default Heading
