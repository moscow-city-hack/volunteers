import React, { useState, useCallback } from 'react'
import Typist from 'react-typist'
import Text from 'components/base/text/Text'
import isString from 'lodash/isString'
import size from 'lodash/size'

export type PrintedTextProps = {
  text: string | Array<string>,
  backspaceDelay?: number,
}

const PrintedText = (props: PrintedTextProps) => {
  const { text = '', backspaceDelay = 500 } = props

  const [value, setValue] = useState(0)

  const content = isString(text) ? text : text[value % size(text)]
  const _onTypingDone = useCallback(() => setValue((v) => v + 1), [setValue])

  return (
    <Typist key={value} onTypingDone={_onTypingDone} className="printed-text">
      <Text type="semibold" variant="xl" Component="span">
        {content}
      </Text>
      <Typist.Backspace count={size(content)} delay={backspaceDelay} />
    </Typist>
  )
}

export default PrintedText
