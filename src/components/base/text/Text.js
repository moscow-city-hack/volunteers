import React, { ElementType, ReactNode, forwardRef, Ref, useMemo } from 'react'
import classNames from 'classnames'
import './text.scss'

export type TextTypes = 'normal' | 'medium' | 'semibold' | 'bold'
export type TextVariants = 'xs' | 'sm' | 'md' | 'lg' | 'xl'

export type TextProps = {
  className?: string,
  children?: ReactNode,
  variant?: TextVariants,
  Component?: ElementType,
  uppercase?: boolean,
  type?: TextTypes,
  center?: boolean,
  right?: boolean,
  crossed?: boolean,
  underline?: boolean,
}

const Text = (props: TextProps, ref: Ref) => {
  const {
    className = '',
    children = null,
    variant = 'md',
    Component = 'div',
    uppercase = false,
    type = 'normal',
    center = false,
    right = false,
    crossed = false,
    underline = false,
    ...other
  } = props

  const weightByType = useMemo(
    () => ({
      normal: 400,
      medium: 500,
      semibold: 600,
      bold: 700,
    }),
    [],
  )

  return (
    <Component
      {...other}
      ref={ref}
      className={classNames([
        'text',
        className,
        type,
        variant ? `text-${variant}` : '',
        { uppercase, center, right, crossed, underline },
      ])}
      style={{ fontWeight: weightByType[type] }}
    >
      {children}
    </Component>
  )
}

export default forwardRef(Text)
