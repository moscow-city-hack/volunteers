import React from 'react'
import Text from 'components/base/text/Text'

export type TextWithSubtextProps = {
  text: string,
  subtext: string,
}

const TextWithSubtext = (props: TextWithSubtextProps) => {
  const { text = '', subtext = '' } = props

  return (
    <div className="text-with-subtext">
      <Text className="text-with-subtext__text" type="semibold" variant="sm">
        {text}
      </Text>
      <Text variant="sm">{subtext}</Text>
    </div>
  )
}

export default TextWithSubtext
