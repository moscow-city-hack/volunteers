import React from 'react'
import size from 'lodash/size'
import BaseCard from 'components/cards/base/BaseCard'
import AvatarBlock from 'components/base/avatar/AvatarBlock'
import TextWithSubtext from 'components/base/text/TextWithSubtext'
import AboutInfoBadge from 'components/base/badge/AboutInfoBadge'
import type { AboutBadgeType } from 'components/base/badge/AboutInfoBadge'
import './about.scss'

export type AboutCardProps = {
  data: Record<string, any>,
  badges?: Array<AboutBadgeType>,
}

const AboutCard = (props: AboutCardProps) => {
  const { data = {}, badges = [] } = props
  const { format = '', work = '' } = data

  return (
    <BaseCard
      className="about"
      renderContent={() => (
        <>
          <AvatarBlock {...data} />
          {size(badges) > 0 && (
            <div className="about__info">
              {badges.map((badge: AboutBadgeType, index) => (
                <AboutInfoBadge key={index} {...badge} />
              ))}
            </div>
          )}
          {size(format) > 0 && <TextWithSubtext text="Формат" subtext={format} />}
          {size(work) > 0 && <TextWithSubtext text="Тип работы" subtext={work} />}
        </>
      )}
    />
  )
}

export default AboutCard
