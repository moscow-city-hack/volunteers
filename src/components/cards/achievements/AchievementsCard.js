import React, { useCallback } from 'react'
import size from 'lodash/size'
import classNames from 'classnames'
import BaseCard from 'components/cards/base/BaseCard'
import Text from 'components/base/text/Text'
import Row from 'components/base/row/Row'
import './achievements.scss'

export type AchievementsCardProps = {
  achievements?: Array<string>,
}

const AchievementsCard = (props: AchievementsCardProps) => {
  const { achievements = [] } = props

  const renderItem = useCallback((text: string, index: number, _, fake: boolean = false) => {
    const content = fake ? 'Еще не|открыто' : text
    const parts = content.split('|')

    return (
      <Row key={index} className="achievements__item">
        {fake ? (
          <div className="achievements__item-fake"></div>
        ) : (
          <img
            className="achievements__item-image"
            src="https://natworld.info/wp-content/uploads/2016/08/Rassvet-skvoz-gustuyu-stenu-lesnyh-derevev.jpg"
            alt=""
          />
        )}
        <div className={classNames('achievements__item-texts', { fake })}>
          <Text variant="sm" Component={Row}>{parts[0] ?? ''}</Text>
          <Text variant="sm" Component={Row}>{parts[1] ?? ''}</Text>
        </div>
      </Row>
    )
  }, [])

  const count = size(achievements)

  return (
    <BaseCard
      className="achievements"
      renderContent={() => (
        <>
          <Row horizontal="between" className="achievements__head">
            <Text type="semibold" variant="">
              {'Достижения'}
            </Text>
            <Text>{[count, 12].join(' из ')}</Text>
          </Row>
          <div className="achievements__content">
            {achievements.map(renderItem)}
            {Array.from(Array(12 - count).keys()).map((text, index, _) => {
              return renderItem(text, index, _, true)
            })}
          </div>
        </>
      )}
    />
  )
}

export default AchievementsCard
