import React, { ReactNode } from 'react'
import isString from 'lodash/isString'
import isFinite from 'lodash/isFinite'
import classNames from 'classnames'
import SvgIcon from 'components/base/icon/SvgIcon'
import Text from 'components/base/text/Text'
import { format } from 'utils/common'
import type { SvgIconType } from 'components/base/icon/SvgIcon'
import './avatar.scss'

export type AvatarCardProps = {
  index?: number,
  avatar?: SvgIconType | string,
  hasNumber?: boolean,
  renderContent?: () => ReactNode,
  avatarPlace?: 'top' | 'left',
  id?: string,
  title?: string,
  description?: string,
  renderAtEnd?: () => ReactNode,
  withShadow?: boolean,
}

const AvatarCard = (props: AvatarCardProps) => {
  const {
    index,
    avatar,
    hasNumber = false,
    renderContent,
    avatarPlace = 'top',
    id,
    title,
    description,
    renderAtEnd,
    withShadow = true,
  } = props

  const hasIndex = hasNumber && isFinite(index)

  return (
    <div className={classNames('avatar', avatarPlace, { shadow: withShadow })}>
      <div className="avatar__block">
        <div className={classNames('avatar__image', id)}>
          {isString(avatar) ? <img src={avatar} /> : <SvgIcon Icon={avatar} />}
        </div>
      </div>
      <div className="avatar__content">
        {renderContent?.() ?? (
          <>
            {Boolean(title) && (
              <Text type="medium" variant="lg" className="avatar__text">
                {title}
              </Text>
            )}
            {Boolean(description) && (
              <Text variant="sm" className="avatar__subtext">
                {description}
              </Text>
            )}
            {renderAtEnd?.()}
          </>
        )}
      </div>
      {hasIndex && <div className="avatar__number">{format(index + 1)}</div>}
    </div>
  )
}

export default AvatarCard
