import React from 'react'
import BaseCard from 'components/cards/base/BaseCard'
import Row from 'components/base/row/Row'
import Text from 'components/base/text/Text'
import AboutInfoBadge from 'components/base/badge/AboutInfoBadge'
import './badge.scss'

export type BadgesCardProps = {
  badges: Array<string>,
  title: string,
}

const BadgesCard = (props: BadgesCardProps) => {
  const { badges = [], title= '' } = props

  return (
    <BaseCard
      className="badges-card"
      renderContent={() => (
        <>
          <Text type="semibold" className="badges-card__title">
            {title}
          </Text>
          <Row wrap className="badges-card__items">
            {badges.map((text, index) => (
              <AboutInfoBadge key={index} text={text} />
            ))}
          </Row>
        </>
      )}
    />
  )
}

export default BadgesCard
