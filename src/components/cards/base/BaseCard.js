import React, { useCallback, useState, useMemo, ReactNode } from 'react'
import { Link } from 'react-router-dom'
import moment from 'moment'
import classNames from 'classnames'
import size from 'lodash/size'
import Text from 'components/base/text/Text'
import Info from 'components/base/info/Info'
import Row from 'components/base/row/Row'
import SvgIcon from 'components/base/icon/SvgIcon'
import { ICONS } from 'assets/icons/icons'
import { DATE_PICKER_DATE_FORMAT } from 'components/inputs/date/DatePicker'
import { DATE_INPUT } from 'utils/date-time'
import { getNounByKey } from 'utils/common'
import type { HelpCardType } from 'components/sections/help/HelpSection'
import type { NewsCardType } from 'components/sections/news/NewsSection'
import './base.scss'

export type BaseCardType = {
  text: string,
  image: string,
  favorite?: boolean,
  link?: string,
  urgently?: boolean,
}

export type BaseCardProps = BaseCardType &
  Partial<HelpCardType> &
  Partial<NewsCardType> & {
    hasFavorite?: boolean,
    isImageFullHeight?: boolean,
    renderContent?: () => ReactNode,
    className?: string,
  }

const BaseCard = (props: BaseCardProps) => {
  const {
    text = '',
    image = '',
    className = '',
    favorite,
    location = '',
    date,
    hasFavorite = true,
    isImageFullHeight = false,
    urgently = false,
    minutes = 0,
    link = '',
    renderContent,
  } = props

  const hasDate = Boolean(date) && Boolean(date?.from) && Boolean(date?.to)

  const [isFavorite, setIsFavorite] = useState(favorite)

  const onClickFavorite = useCallback(() => {
    setIsFavorite((p) => !p)
    // TODO: Отправить данные на бэк
  }, [setIsFavorite])

  const getDateString = useCallback(
    (date: string) => moment(date, DATE_PICKER_DATE_FORMAT).format('ll'),
    [],
  )

  const title = useMemo(
    () =>
      size(text) > 0 ? (
        <Text variant="lg" type="semibold" className="card__text">
          {text}
        </Text>
      ) : null,
    [text],
  )

  const photo = useMemo(
    () =>
      size(image) > 0 ? (
        <img
          src={image}
          alt=""
          className={classNames('card__image', { 'full-image': isImageFullHeight })}
        />
      ) : null,
    [image, isImageFullHeight],
  )

  return (
    <div className={classNames('card', className)}>
      {renderContent?.() ?? (
        <>
          {size(link) > 0 ? (
            <Row className="card__container">
              <Link to={link} className="card__link" />
              {photo}
            </Row>
          ) : (
            photo
          )}
          {hasFavorite && (
            <SvgIcon
              Icon={ICONS.heart}
              className={classNames('card__favorite', { active: isFavorite })}
              onClick={onClickFavorite}
            />
          )}
          {urgently && (
            <Text type="semibold" variant="lg" className="card__urgently">
              {'Срочно'}
            </Text>
          )}
          <div className="card__content">
            {size(link) > 0 ? <Link to={link}>{title}</Link> : title}
            {size(location) > 0 && (
              <Info
                id="location"
                text={location}
                Component="a"
                href={`https://yandex.ru/maps/?text=${location}`}
                target="_blank"
              />
            )}
            {hasDate && (
              <Info
                className="card__date"
                id="calendar"
                text={[getDateString(date.from), getDateString(date.to)].join(
                  DATE_INPUT.RANGE_DELIMITER,
                )}
              />
            )}
            {minutes > 0 && (
              <Info
                id="eyeOpened"
                text={[getNounByKey('minutes', minutes, true), 'на чтение'].join(' ')}
              />
            )}
          </div>
        </>
      )}
    </div>
  )
}

export default BaseCard
