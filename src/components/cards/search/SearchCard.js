import React, { useState, useCallback } from 'react'
import Input from 'components/inputs/input/Input'
import Button from 'components/base/button/Button'
import Checkbox from 'components/checkboxes/Checkbox'
import Heading from 'components/base/text/Heading'
import Row from 'components/base/row/Row'
import BaseExtendedSelect from 'components/selects/BaseExtendedSelect'
import {
  VOLUNTEERS,
  ORGANISATIONS,
  EVENTS,
} from 'components/sections/search/SearchSection'
import { API_PATHS } from 'network/apiPaths'
import type { CheckboxCallbackData } from 'components/checkboxes/Checkbox'
import type { InputCallbackData } from 'components/inputs/input/Input'
import './search.scss'

export type SearchCardProps = {
  place: VOLUNTEERS | ORGANISATIONS | EVENTS,
}

const SearchCard = (props: SearchCardProps) => {
  const { place } = props

  const isCompactView = [ORGANISATIONS, VOLUNTEERS].includes(place)

  const [search, setSearch] = useState('')
  const [date, setDate] = useState([])
  const [time, setTime] = useState('')
  const [timeEnd, setTimeEnd] = useState('')

  const [isRangeDate, setIsRangeDate] = useState(false)
  const [isRangeTime, setIsRangeTime] = useState(false)

  const onChangeSearch = useCallback(
    ({ value }: InputCallbackData) => setSearch(value),
    [setSearch],
  )

  const onResetSearch = useCallback(() => setSearch(''), [setSearch])

  const onChangeDate = useCallback(
    ({ value }: InputCallbackData) => setDate(value),
    [setDate],
  )

  const onResetDate = useCallback(
    () => setDate(isRangeDate ? { from: '', to: '' } : []),
    [setDate, isRangeDate],
  )

  const onChangeDateCheckbox = useCallback(
    ({ value }: CheckboxCallbackData) => {
      setIsRangeDate(value)
      onResetDate()
    },
    [onResetDate, setIsRangeDate],
  )

  const onChangeTime = useCallback(
    ({ value }: InputCallbackData) => console.log('time', value) || setTime(value),
    [setTime],
  )

  const onChangeTimeEnd = useCallback(
    ({ value }: InputCallbackData) => setTimeEnd(value),
    [setTimeEnd],
  )

  const onResetTime = useCallback(() => setTime(''), [setTime])
  const onResetTimeEnd = useCallback(() => setTimeEnd(''), [setTimeEnd])

  const onChangeTimeCheckbox = useCallback(
    ({ value }: CheckboxCallbackData) => {
      setIsRangeTime(value)
      onResetTime()
      onResetTimeEnd()
    },
    [onResetTime, onResetTimeEnd, setIsRangeTime],
  )

  const [categories, setCategories] = useState(null)

  const onChangeCategories = useCallback(
    ({ value }: Record<string, any>) => setCategories(value),
    [setCategories],
  )

  return (
    <>
      <div className="search-wrapper">
        <Row>
          <Input
            type="text"
            val={search}
            onChange={onChangeSearch}
            onReset={onResetSearch}
            placeholder="Поиск"
          />
          <Button text="Найти" size="middle" />
        </Row>
        {!isCompactView && (
          <>
            <Row>
              <Input
                type="date"
                val={date}
                onChange={onChangeDate}
                onReset={onResetDate}
                isRangeDate={isRangeDate}
                isArrayDate={!isRangeDate}
                allowEmptyValue
                placeholder="Даты проведения"
              />
              <Checkbox
                id="date"
                value={isRangeDate}
                onChange={onChangeDateCheckbox}
                label="Диапазон дат"
              />
            </Row>
            <Row>
              <Input
                type="time"
                val={time}
                onChange={onChangeTime}
                onReset={onResetTime}
                placeholder={isRangeTime ? 'Время начала' : 'Время'}
              />
              <Checkbox
                id="time"
                value={isRangeTime}
                onChange={onChangeTimeCheckbox}
                label="Диапазон времени"
              />
            </Row>
            {isRangeTime && (
              <Row>
                <Input
                  type="time"
                  val={timeEnd}
                  onChange={onChangeTimeEnd}
                  onReset={onResetTimeEnd}
                  placeholder="Время окончания"
                />
                <div className="filler"></div>
              </Row>
            )}
          </>
        )}
      </div>
      <div className="search-wrapper">
        <Heading text="Категории" size="small" withSpacing={false} />
        <BaseExtendedSelect
          serviceSearch={API_PATHS.GET_CATEGORIES}
          value={categories}
          onChange={onChangeCategories}
          placeholder="Категории"
          defaultOptions={[
            { displayname: 'Животные', id: 'animals', hasIcon: true },
            { displayname: 'Поиск пропавших', id: 'search', hasIcon: true },
            { displayname: 'Помощь природе', id: 'nature', hasIcon: true },
            { displayname: 'Образование', id: 'education', hasIcon: true },
          ]}
        />
      </div>
    </>
  )
}

export default SearchCard
