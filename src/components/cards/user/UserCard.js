import React from 'react'
import classNames from 'classnames'
import BaseCard from 'components/cards/base/BaseCard'
import AvatarBlock from 'components/base/avatar/AvatarBlock'
import Row from 'components/base/row/Row'
import Button from 'components/base/button/Button'
import type { MapItemType } from 'components/map/BasicMap'
import { stopPropagationEvent } from 'utils/common'
import './user.scss'

export type UserType = {
  name: string,
  id: string | number,
  rating: number,
  reviews: number,
  image: string,
  place: MapItemType,
}

export type UserCardProps = UserType & {
  onClick?: () => void,
}

const UserCard = (props: UserCardProps) => {
  const { place, isCompact = false, isActive = false, onClick, ...other } = props

  return (
    <BaseCard
      className={classNames('user', {
        'user--compact': isCompact,
        'user--active': isActive,
      })}
      renderContent={() => (
        <Row horizontal="between" className="user__content" onClick={onClick}>
          <AvatarBlock {...other} />
          <Row className="user__buttons">
            <Button text="Нанять" size="small" onClick={stopPropagationEvent} />
            <Button
              text="Написать"
              size="small"
              variant="outline"
              onClick={stopPropagationEvent}
            />
          </Row>
        </Row>
      )}
    />
  )
}

export default UserCard
