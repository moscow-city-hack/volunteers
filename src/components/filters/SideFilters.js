import React, { useMemo } from 'react'
import size from 'lodash/size'
import PlaceFilter from 'components/filters/place/PlaceFilter'
import FormatFilter from 'components/filters/format/FormatFilter'
import ConditionsFilter from 'components/filters/conditions/ConditionsFilter'
import AgeFilter from 'components/filters/age/AgeFilter'
import RolesFilter from 'components/filters/roles/RolesFilter'
import SkillsFilter from 'components/filters/skills/SkillsFilter'
import WorkFilter from 'components/filters/work/WorkFilter'
import RatingFilter from 'components/filters/rating/RatingFilter'
import UrgencyFilter from 'components/filters/urgency/UrgencyFilter'
import MotivationFilter from 'components/filters/motivation/MotivationFilter'
import OrganisationFilter from 'components/filters/organisation/OrganisationFilter'
import './filters.scss'

export type SideFiltersProps = {
  filters: Array<string>,
}

export const FILTERS_TYPES = {
  place: 'place',
  format: 'format',
  conditions: 'conditions',
  age: 'age',
  roles: 'roles',
  skills: 'skills',
  work: 'work',
  rating: 'rating',
  urgency: 'urgency',
  motivation: 'motivation',
  organisation: 'organisation',
}

const SideFilters = (props: SideFiltersProps) => {
  const { filters = [] } = props

  const renderFilterByName = useMemo(
    () => ({
      [FILTERS_TYPES.place]: (key: string) => <PlaceFilter key={key} />,
      [FILTERS_TYPES.format]: (key: string) => <FormatFilter key={key} />,
      [FILTERS_TYPES.conditions]: (key: string) => <ConditionsFilter key={key} />,
      [FILTERS_TYPES.age]: (key: string) => <AgeFilter key={key} />,
      [FILTERS_TYPES.roles]: (key: string) => <RolesFilter key={key} />,
      [FILTERS_TYPES.skills]: (key: string) => <SkillsFilter key={key} />,
      [FILTERS_TYPES.work]: (key: string) => <WorkFilter key={key} />,
      [FILTERS_TYPES.rating]: (key: string) => <RatingFilter key={key} />,
      [FILTERS_TYPES.urgency]: (key: string) => <UrgencyFilter key={key} />,
      [FILTERS_TYPES.motivation]: (key: string) => <MotivationFilter key={key} />,
      [FILTERS_TYPES.organisation]: (key: string) => <OrganisationFilter key={key} />,
    }),
    [],
  )

  return size(filters) > 0 ? (
    <div className="filters">
      {filters.map((name: string, index) => {
        const renderFilter = renderFilterByName[name]
        return renderFilter?.([name, index].join('_'))
      })}
    </div>
  ) : null
}

export default SideFilters
