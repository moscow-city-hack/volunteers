import React from 'react'
import NumberRangeFilter from 'components/filters/base/NumberRangeFilter'
import 'components/filters/filters.scss'

const AgeFilter = () => {
  return <NumberRangeFilter title="Возраст" />
}

export default AgeFilter
