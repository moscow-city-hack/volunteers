import React, { useState, useCallback } from 'react'
import BaseCard from 'components/cards/base/BaseCard'
import FilterCardHeading from 'components/base/text/FilterCardHeading'
import MultiCheckbox from 'components/checkboxes/MultiCheckbox'
import 'components/filters/filters.scss'

export type CheckboxesFilterProps = {
  checkboxes: Array<{ id: string, label: string }>,
}

const CheckboxesFilter = (props: CheckboxesFilterProps) => {
  const { checkboxes, title = '' } = props

  const [activeCheckboxes, setActiveCheckboxes] = useState([])

  const onChangeCheckbox = useCallback(
    ({ id, value }: CheckboxCallbackData) => {
      setActiveCheckboxes(
        value
          ? [...activeCheckboxes, id]
          : activeCheckboxes.filter((checkbox) => checkbox !== id),
      )
    },
    [activeCheckboxes],
  )

  return (
    <BaseCard
      className="filter"
      renderContent={() => (
        <>
          <FilterCardHeading text={title} />
          <MultiCheckbox
            position="vertical"
            onChange={onChangeCheckbox}
            values={checkboxes.map((checkbox) => {
              return { ...checkbox, value: activeCheckboxes.includes(checkbox.id) }
            })}
          />
        </>
      )}
    />
  )
}

export default CheckboxesFilter
