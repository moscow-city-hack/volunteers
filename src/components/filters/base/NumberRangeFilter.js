import React, { useState, useCallback } from 'react'
import Input from 'components/inputs/input/Input'
import BaseCard from 'components/cards/base/BaseCard'
import FilterCardHeading from 'components/base/text/FilterCardHeading'
import type { InputCallbackData } from 'components/inputs/input/Input'
import 'components/filters/filters.scss'

export type NumberRangeFilterProps = {
  title?: string,
  max?: number,
}

const NumberRangeFilter = (props: NumberRangeFilterProps) => {
  const { title = '', max = 100 } = props

  const [from, setFrom] = useState('')
  const [to, setTo] = useState('')

  const onChangeFrom = useCallback(
    ({ value }: InputCallbackData) => setFrom(value),
    [setFrom],
  )

  const onChangeTo = useCallback(({ value }: InputCallbackData) => setTo(value), [setTo])

  const onResetFrom = useCallback(() => setFrom(''), [setFrom])
  const onResetTo = useCallback(() => setTo(''), [setTo])

  return (
    <BaseCard
      className="filter"
      renderContent={() => (
        <>
          <FilterCardHeading text={title} />
          <Input
            type="counter"
            val={from}
            onChange={onChangeFrom}
            onReset={onResetFrom}
            placeholder="От..."
            intMinValue={1}
            intMaxValue={max}
          />
          <Input
            type="counter"
            val={to}
            onChange={onChangeTo}
            onReset={onResetTo}
            placeholder="До..."
            intMinValue={1}
            intMaxValue={max}
          />
        </>
      )}
    />
  )
}

export default NumberRangeFilter
