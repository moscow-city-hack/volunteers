import React, { useState, useCallback } from 'react'
import BaseCard from 'components/cards/base/BaseCard'
import FilterCardHeading from 'components/base/text/FilterCardHeading'
import BaseExtendedSelect from 'components/selects/BaseExtendedSelect'
import 'components/filters/filters.scss'

export type SelectFilterProps = {
  title?: string,
  serviceSearch?: string,
  defaultOptions?: Array<{ displayname: string, id: string }>,
}

const SelectFilter = (props: SelectFilterProps) => {
  const { title = '', serviceSearch = '', defaultOptions } = props

  const [items, setItems] = useState(null)

  const onChangeItems = useCallback(
    ({ value }: Record<string, any>) => setItems(value),
    [setItems],
  )

  return (
    <BaseCard
      className="filter"
      renderContent={() => (
        <>
          <FilterCardHeading text={title} />
          <BaseExtendedSelect
            serviceSearch={serviceSearch}
            value={items}
            onChange={onChangeItems}
            placeholder={title}
            defaultOptions={defaultOptions}
          />
        </>
      )}
    />
  )
}

export default SelectFilter
