import React from 'react'
import SelectFilter from 'components/filters/base/SelectFilter'
import { API_PATHS } from 'network/apiPaths'
import 'components/filters/filters.scss'

const ConditionsFilter = () => {
  return (
    <SelectFilter
      title="Условия"
      serviceSearch={API_PATHS.GET_CONDITIONS}
      defaultOptions={[
        { displayname: 'Вода', id: 'water' },
        { displayname: 'Еда', id: 'food' },
      ]}
    />
  )
}

export default ConditionsFilter
