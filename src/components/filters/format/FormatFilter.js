import React from 'react'
import CheckboxesFilter from 'components/filters/base/CheckboxesFilter'
import 'components/filters/filters.scss'

const FormatFilter = () => {
  return (
    <CheckboxesFilter
      checkboxes={[
        { id: 'online', label: 'Онлайн' },
        { id: 'offline', label: 'Оффлайн' },
      ]}
      title="Формат"
    />
  )
}

export default FormatFilter
