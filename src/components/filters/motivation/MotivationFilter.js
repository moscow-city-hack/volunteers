import React from 'react'
import SelectFilter from 'components/filters/base/SelectFilter'
import { API_PATHS } from 'network/apiPaths'

const MotivationFilter = () => {
  return (
    <SelectFilter
      title="Мотивация"
      serviceSearch={API_PATHS.GET_MOTIVATIONS}
      defaultOptions={[{ displayname: 'Деньги', id: 'money' }]}
    />
  )
}

export default MotivationFilter
