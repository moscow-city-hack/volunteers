import React from 'react'
import SelectFilter from 'components/filters/base/SelectFilter'
import { API_PATHS } from 'network/apiPaths'
import 'components/filters/filters.scss'

const OrganisationFilter = () => {
  return (
    <SelectFilter
      title="Тип"
      serviceSearch={API_PATHS.GET_ORGTYPES}
      defaultOptions={[
        { displayname: 'ООО', id: 'ooo' },
        { displayname: 'ОАО', id: 'oao' },
      ]}
    />
  )
}

export default OrganisationFilter
