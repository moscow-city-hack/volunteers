import React, { useState, useCallback } from 'react'
import Input from 'components/inputs/input/Input'
import type { InputCallbackData } from 'components/inputs/input/Input'
import 'components/filters/filters.scss'

const PlaceFilter = () => {
  const [place, setPlace] = useState('')

  const onChangePlace = useCallback(
    ({ value }: InputCallbackData) => setPlace(value),
    [setPlace],
  )

  const onResetPlace = useCallback(() => setPlace(''), [setPlace])

  return (
    <Input
      type="text"
      val={place}
      onChange={onChangePlace}
      onReset={onResetPlace}
      placeholder="Место"
    />
  )
}

export default PlaceFilter
