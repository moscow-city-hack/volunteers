import React from 'react'
import NumberRangeFilter from 'components/filters/base/NumberRangeFilter'
import 'components/filters/filters.scss'

const RatingFilter = () => {
  return <NumberRangeFilter title="Рейтинг" max={5} />
}

export default RatingFilter
