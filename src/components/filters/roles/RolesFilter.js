import React from 'react'
import SelectFilter from 'components/filters/base/SelectFilter'
import { API_PATHS } from 'network/apiPaths'

const RolesFilter = () => {
  return (
    <SelectFilter
      title="Роли"
      serviceSearch={API_PATHS.GET_ROLES}
      defaultOptions={[
        { displayname: 'Менеджер', id: 'manager' },
        { displayname: 'Бухгалтер', id: 'accountant' },
      ]}
    />
  )
}

export default RolesFilter
