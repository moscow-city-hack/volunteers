import React from 'react'
import SelectFilter from 'components/filters/base/SelectFilter'
import { API_PATHS } from 'network/apiPaths'

const SkillsFilter = () => {
  return (
    <SelectFilter
      title="Навыки"
      serviceSearch={API_PATHS.GET_SKILLS}
      defaultOptions={[{ displayname: 'Программист', id: 'programmer' }]}
    />
  )
}

export default SkillsFilter
