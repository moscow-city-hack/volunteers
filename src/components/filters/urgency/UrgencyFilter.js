import React from 'react'
import CheckboxesFilter from 'components/filters/base/CheckboxesFilter'
import 'components/filters/filters.scss'

const UrgencyFilter = () => {
  return (
    <CheckboxesFilter
      checkboxes={[{ id: 'urgently', label: 'Срочно' }]}
      title="Срочность"
    />
  )
}

export default UrgencyFilter
