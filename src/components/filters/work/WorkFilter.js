import React from 'react'
import CheckboxesFilter from 'components/filters/base/CheckboxesFilter'
import 'components/filters/filters.scss'

const WorkFilter = () => {
  return (
    <CheckboxesFilter
      checkboxes={[
        { id: 'temporary', label: 'Временная' },
        { id: 'permanent', label: 'Постоянная' },
        { id: 'remote', label: 'Удаленная' },
        { id: 'internship', label: 'Стажировка' },
      ]}
      title="Тип работы"
    />
  )
}

export default WorkFilter
