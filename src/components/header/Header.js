import React, { useCallback } from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'
import size from 'lodash/size'
import Menu from 'components/menu/Menu'
import Button from 'components/base/button/Button'
import Tooltip from 'components/base/tooltip/Tooltip'
import Row from 'components/base/row/Row'
import logo from 'assets/images/logo.png'
import Text from 'components/base/text/Text'
import SvgIcon from 'components/base/icon/SvgIcon'
import { logout } from 'network/requests'
import { ICONS } from 'assets/icons/icons'
import { SettingsSelectors } from 'store/Settings/reducers'
import { setLogged } from 'store/Settings/actions'
import './header.scss'

export type HeaderProps = {
  active?: string,
}

const Header = (props: HeaderProps) => {
  const { active = '', isLoggedIn, setLogged, data = {} } = props
  const { name = '', image = '' } = data

  const onLogout = useCallback(() => {
    logout().then(({ isLoggedIn }) => setLogged(isLoggedIn))
  }, [setLogged])

  return (
    <Row className="header-wrapper" horizontal="between">
      <Row className="header">
        <Row horizontal="between" className="header__content">
          <Link to="/">
            <img src={logo} className="header__logo" alt="Logo" />
          </Link>
          <Menu active={active} />
          {isLoggedIn ? (
            <Row className="header__logged">
              <Tooltip title="Выйти из аккаунта">
                <Link to="/" onClick={onLogout}>
                  <SvgIcon Icon={ICONS.logout} className="header__logout" />
                </Link>
              </Tooltip>
              <Row Component={Link} to="/personal" className="header__avatar-wrapper">
                <div className="header__avatar">
                  {size(image) > 0 ? (
                    <img src={image} alt="" />
                  ) : (
                    <SvgIcon Icon={ICONS.user} />
                  )}
                </div>
                <Text>{name}</Text>
              </Row>
            </Row>
          ) : (
            <Row horizontal="between" className="header__inner">
              <Link to="/login" className="header__button">
                <Button text="Войти" variant="outline" size="middle" />
              </Link>
              <Link to="/register" className="header__button">
                <Button text="Зарегистрироваться" size="middle" />
              </Link>
            </Row>
          )}
        </Row>
      </Row>
    </Row>
  )
}

const mapStateToProps = (state) => ({
  isLoggedIn: SettingsSelectors.getIsLogged(state),
  data: SettingsSelectors.getData(state),
})

const mapActionsToProps = {
  setLogged,
}

export default connect(mapStateToProps, mapActionsToProps)(Header)
