import React, { useEffect, useRef } from 'react'
import classNames from 'classnames'
import get from 'lodash/get'
import './pickers.scss'

export type PickerListItemtype = { value: number | string, title: string }

export type PickerListProps = {
  className?: string,
  label?: string,
  value: number | string,
  items: Array<PickerListItemtype>,
  onClick: (value: number) => void,
  defaultScrollValue: number | string,
}

const PickerList = (props: PickerListProps) => {
  const { className = '', label, items, value, onClick, defaultScrollValue } = props
  const list = useRef()
  const selectItems = useRef({})

  useEffect(() => {
    // Скролл до активных элементов
    const _list = list.current
    const selectItem = get(selectItems.current, value || defaultScrollValue)

    if (_list && selectItem) {
      const top = selectItem.offsetTop - _list.offsetTop
      const height = (selectItem.clientHeight - _list.clientHeight) / 2.0

      _list.scrollTop = top + height
    }
  }, [defaultScrollValue, value])

  return (
    <div className={classNames(['picker-list', className])}>
      {label && <div className="picker-list__label">{label}</div>}
      <div ref={list} className="picker-list__items">
        {items.map((i) => {
          const isActive = value && i.value.toString() === value.toString()

          return (
            <div
              ref={(ref) => (selectItems.current[i.value] = ref)}
              key={`Item_${i.value}`}
              onClick={() => onClick(i.value)}
              className={classNames({
                'picker-list__item': true,
                'picker-list__item--active': isActive,
              })}
            >
              {i.title}
            </div>
          )
        })}
      </div>
    </div>
  )
}

export default PickerList
