import React, { useCallback } from 'react'
import padStart from 'lodash/padStart'
import moment, { Moment } from 'moment'
import Popover, { PopoverProps } from '@material-ui/core/Popover/Popover'
import PickerList from 'components/inputs/pickers/PickerList'
import { stopPropagationEvent } from 'utils/common'
import './time.scss'

export const TIME_PICKER_FORMAT = 'HH:mm'

type SelectId = 'hours' | 'minutes'

export type TimePickerProps = {
  value: string,
  visible: boolean,
  onChange: (data: string) => void,
} & $Shape<PopoverProps>

const TimePicker = (props: TimePickerProps) => {
  const {
    value,
    visible,
    anchorEl,
    onClose,
    anchorOrigin = { vertical: 'bottom', horizontal: 'right' },
    transformOrigin = { vertical: 'top', horizontal: 'right' },
    onChange,
  } = props

  const getSelectIndexOptions = useCallback((count: number, padLength: number = 0) => {
    return Array.from(Array(count).keys()).map((v) => ({
      value: v,
      title: padLength ? padStart(v, padLength, '0') : v + 1,
    }))
  }, [])

  const momentValue = moment(value, TIME_PICKER_FORMAT)

  const getSubValue = useCallback(
    (id: SelectId) => (momentValue.isValid() ? momentValue.get(id) : ''),
    [momentValue],
  )

  const renderSelect = useCallback(
    (
      id: SelectId,
      label: string,
      count: number,
      momentValue: Moment,
      localValue: string,
    ) => {
      const items = getSelectIndexOptions(count, 2)

      return (
        <PickerList
          items={items}
          label={label}
          value={localValue}
          onClick={(value) => {
            const resultMoment = momentValue.isValid() ? momentValue : moment()
            const resultValue = resultMoment.set(id, value).format(TIME_PICKER_FORMAT)

            onChange(resultValue)
          }}
        />
      )
    },
    [onChange, getSelectIndexOptions],
  )

  return (
    <Popover
      classes={{ paper: 'time-picker_paper' }}
      open={visible}
      anchorEl={anchorEl.current}
      anchorOrigin={anchorOrigin}
      transformOrigin={transformOrigin}
      onBackdropClick={onClose}
    >
      <div onClick={stopPropagationEvent} className="time-picker__container">
        {renderSelect('hours', 'Часы', 24, momentValue, getSubValue('hours'))}
        {renderSelect('minutes', 'Минуты', 60, momentValue, getSubValue('minutes'))}
      </div>
    </Popover>
  )
}

export default TimePicker
