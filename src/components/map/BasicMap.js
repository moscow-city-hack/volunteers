import React, { useCallback, useRef, useState } from 'react'
import classNames from 'classnames'
import { Placemark, YMaps, Map } from 'react-yandex-maps'
import Preloader from 'components/base/preloader/Preloader'
import type { UserType } from 'components/cards/user/UserCard'
import './map.scss'

export type MapItemType = { latitude: number, longitude: number }

export type BasicMapProps = {
  className?: string,
  users: Array<MapItemType>,
  onClickPlace?: (user: UserType, index: number) => void,
  activeId?: number | null,
}

const BasicMap = (props: BasicMapProps) => {
  const { className = '', users = [], onClickPlace, activeId } = props

  const ymaps = useRef(null)

  const [isFetching, setIsFetching] = useState(true)

  const onLoad = useCallback((_map) => {
    setIsFetching(false)
    ymaps.current = _map
  }, [])

  const getPoint = useCallback(
    ({ latitude, longitude }: MapItemType) => [latitude, longitude],
    [],
  )

  return (
    <YMaps>
      <div className={classNames('map', className)}>
        {isFetching && <Preloader />}
        <Map
          onLoad={onLoad}
          width="100%"
          height="100%"
          modules={['control.ZoomControl']}
          options={{
            autoFitToViewport: 'always',
            suppressMapOpenBlock: true,
          }}
          defaultState={{
            center: [55.751574, 37.573856],
            zoom: 9,
          }}
        >
          {users?.map((user, index) => {
            const point = getPoint(user.place)
            const isActive = activeId === user.id

            return (
              <Placemark
                key={index}
                geometry={point}
                onClick={() => onClickPlace?.(user, index)}
                properties={{
                  iconContent: `<div class="map-icon">
                                  <svg width="32" height="32" viewBox="0 0 32 32" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path fill-rule="evenodd" clip-rule="evenodd" d="M28 13.3334C28 22.6667 16 30.6667 16 30.6667C16 30.6667 4 22.6667 4 13.3334C4 6.70596 9.37258 1.33337 16 1.33337C22.6274 1.33337 28 6.70596 28 13.3334Z" fill="${
                                      isActive ? '#EB0000' : '#2D39E1'
                                    }"/>
                                    <path fill-rule="evenodd" clip-rule="evenodd" d="M16 17.3334C18.2091 17.3334 20 15.5425 20 13.3334C20 11.1242 18.2091 9.33337 16 9.33337C13.7909 9.33337 12 11.1242 12 13.3334C12 15.5425 13.7909 17.3334 16 17.3334Z" fill="${
                                      isActive ? '#EB0000' : '#2D39E1'
                                    }" stroke="white" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                                  </svg>
                                </div>`,
                }}
              />
            )
          })}
        </Map>
      </div>
    </YMaps>
  )
}

export default BasicMap
