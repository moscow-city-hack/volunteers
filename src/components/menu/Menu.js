import React, { useCallback, useState, useMemo } from 'react'
import { Link } from 'react-router-dom'
import classNames from 'classnames'
import Text from 'components/base/text/Text'
import SvgIcon from 'components/base/icon/SvgIcon'
import Row from 'components/base/row/Row'
import { ICONS } from 'assets/icons/icons'
import type { MenuItemType } from 'components/menu/ExtendedMenu'
import './menu.scss'

export type MenuProps = {
  active?: string,
}

const Menu = (props: MenuProps) => {
  const { active = '' } = props

  const [isMenuVisible, setIsMenuVisible] = useState(false)

  const menu = useMemo(
    () => [
      { text: 'Волонтеры', link: '/volunteers', id: 'volunteers' },
      { text: 'Организации', link: '/organisations', id: 'organisations' },
      { text: 'Добрые дела', link: '/events', id: 'events' },
    ],
    [],
  )

  const renderMenu = useCallback(
    () => (
      <Row
        horizontal="between"
        Component="ul"
        className={classNames('menu__list', { active: isMenuVisible })}
      >
        {menu.map(({ text, icon, link, id }: MenuItemType) => (
          <li className="menu__item" key={text}>
            <Row
              horizontal="between"
              Component={link ? Link : 'span'}
              to={link}
              className={classNames('menu__link', { active: active === id })}
            >
              {Boolean(icon) && <SvgIcon Icon={ICONS[icon]} />}
              <Text type="medium" className="menu__text">
                {text}
              </Text>
            </Row>
          </li>
        ))}
      </Row>
    ),
    [menu, isMenuVisible, active],
  )

  const onMenuButtonClick = useCallback(() => {
    setIsMenuVisible((p) => !p)
    document.querySelector('html').classList.toggle('scroll')
  }, [setIsMenuVisible])

  return (
    <nav className={classNames('menu', { active: isMenuVisible })}>
      <div
        className={classNames('menu__btn', { active: isMenuVisible })}
        onClick={onMenuButtonClick}
      >
        <span></span>
      </div>
      {renderMenu()}
    </nav>
  )
}

export default Menu
