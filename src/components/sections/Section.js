import React, { ReactNode, CSSProperties } from 'react'
import classNames from 'classnames'
import './section.scss'

export type SectionProps = {
  children: ReactNode,
  type?: 'image',
  className?: string,
  style?: CSSProperties,
  fullWidth?: boolean,
  Component?: string | ReactNode,
  ContainerComponent?: string | ReactNode,
  containerProps?: Record<string, any>,
}

const Section = (props: SectionProps) => {
  const {
    children,
    type,
    className = '',
    style = {},
    fullWidth = false,
    Component = 'section',
    ContainerComponent = 'div',
    containerProps = {},
  } = props

  return (
    <Component
      className={classNames('section', className, {
        'section--image': type === 'image',
      })}
      style={style}
    >
      {fullWidth ? (
        children
      ) : (
        <ContainerComponent className="container" {...containerProps}>
          {children}
        </ContainerComponent>
      )}
    </Component>
  )
}

export default Section
