import React, { ReactNode, useMemo } from 'react'
import size from 'lodash/size'
import Section from 'components/sections/Section'
import Heading from 'components/base/text/Heading'
import BaseSlider from 'components/slider/BaseSlider'
import { sliderBreakPoints } from 'utils/mq'
import type { SectionProps } from 'components/sections/Section'

export type TitleSliderSectionProps = {
  items: Array<any>,
  title: string,
  className?: string,
  breakPoints?: Record<number, number | { slidesPerView: number, spaceBetween: number }>,
  renderItem: (item: any, index: number, slidesCount: number) => ReactNode,
  renderAtStart?: () => ReactNode,
  sectionProps?: SectionProps,
  hasFullScreen?: boolean,
}

const TitleSliderSection = (props: TitleSliderSectionProps) => {
  const {
    items = [],
    title = '',
    className = '',
    breakPoints,
    renderItem,
    renderAtStart,
    sectionProps = {},
    hasFullScreen = false,
  } = props

  const breaks = useMemo(() => sliderBreakPoints(breakPoints), [breakPoints])

  if (size(items) === 0) {
    return null
  }

  return (
    <Section className={className} {...sectionProps}>
      {renderAtStart?.()}
      <Heading text={title} />
      <BaseSlider
        swiperClassName={`${className}__swiper`}
        items={items}
        breakPoints={breaks}
        renderItem={renderItem}
        hasPagination
        hasFullScreen={hasFullScreen}
      />
    </Section>
  )
}

export default TitleSliderSection
