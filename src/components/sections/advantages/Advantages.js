import React, { useMemo } from 'react'
import size from 'lodash/size'
import Row from 'components/base/row/Row'
import AvatarCard from 'components/cards/avatar/AvatarCard'
import { ICONS } from 'assets/icons/icons'
import './advantages.scss'

export type AdvantagesItemType = {
  id: string,
  title: string,
  description: string,
}

export type AdvantagesProps = {
  items: Array<AdvantagesItemType>,
  withoutIndices?: boolean,
}

const Advantages = (props: AdvantagesProps) => {
  const { items = [], withoutIndices = false } = props

  return size(items) > 0 ? (
    <Row horizontal="between" vertical="stretch" className="advantages__items">
      {items.map(({ id, title, description }: AdvantagesItemType, index) => (
        <AvatarCard
          id={id}
          key={id}
          index={index}
          avatar={ICONS[id]}
          hasNumber={!withoutIndices}
          title={title}
          description={description}
        />
      ))}
    </Row>
  ) : null
}

export default Advantages
