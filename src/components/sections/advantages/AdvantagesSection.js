import React from 'react'
import Row from 'components/base/row/Row'
import Heading from 'components/base/text/Heading'
import Section from 'components/sections/Section'
import Advantages from 'components/sections/advantages/Advantages'
import type { AdvantagesItemType } from 'components/sections/advantages/Advantages'
import './advantages.scss'

export type AdvantagesSectionProps = {
  items: Array<AdvantagesItemType>,
}

const AdvantagesSection = (props: AdvantagesSectionProps) => {
  const { items = [] } = props

  return (
    <Section className="advantages">
      <Row horizontal="between">
        <Heading text='Преимущества' />
      </Row>
      <Advantages items={items} />
    </Section>
  )
}

export default AdvantagesSection
