import React from 'react'
import Row from 'components/base/row/Row'
import Text from 'components/base/text/Text'
import Menu from 'components/menu/Menu'
import Section from 'components/sections/Section'
import './footer.scss'

const FooterSection = () => {
  return (
    <Section className="footer" Component="footer">
      <Row horizontal="between">
        <Menu />
        <Text>{'© 2022 Все права защищены'}</Text>
      </Row>
    </Section>
  )
}

export default FooterSection
