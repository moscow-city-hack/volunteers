import React from 'react'
import { Link } from 'react-router-dom'
import Section from 'components/sections/Section'
import PrintedText from 'components/base/text/PrintedText'
import Row from 'components/base/row/Row'
import Button from 'components/base/button/Button'
import './head.scss'

const HeadSection = () => {
  return (
    <Section Component="header" className="head">
      <div className="head__content">
        <PrintedText
          text={[
            'На нашей платформе зарегистрировано 35 000 волонтеров',
            'Мы ежедневно помогаем тысячам людей',
            'Благодаря нам были сделаны тысячи добрых дел',
          ]}
        />
        <Row>
          <Link to="/events" className="head__button">
            <Button text="Могу помочь" stretch />
          </Link>
          <Link to="/volunteers" className="head__button">
            <Button text="Нужна помощь" variant="outline" stretch />
          </Link>
        </Row>
      </div>
    </Section>
  )
}

export default HeadSection
