import React from 'react'
import TitledGridSection from 'components/sections/titled-grid/TitledGridSection'
import type { BaseCardType } from 'components/cards/base/BaseCard'
import './help.scss'

export type HelpCardType = BaseCardType & {
  location: string,
  date: { from: string, to: string },
}

export type HelpSectionProps = {
  items: Array<HelpCardType>,
}

const HelpSection = (props: HelpSectionProps) => {
  const { items = [] } = props

  return (
    <TitledGridSection
      className="help"
      title="Можете помочь"
      cards={items}
      link="/events"
    />
  )
}

export default HelpSection
