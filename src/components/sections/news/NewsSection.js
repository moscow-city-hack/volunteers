import React from 'react'
import TitledGridSection from 'components/sections/titled-grid/TitledGridSection'
import type { BaseCardType } from 'components/cards/base/BaseCard'
import './news.scss'

export type NewsCardType = BaseCardType & {
  minutes: number,
}

export type NewsSectionProps = {
  items: Array<NewsCardType>,
}

const NewsSection = (props: NewsSectionProps) => {
  const { items = [] } = props

  return (
    <TitledGridSection
      className="news"
      title="Новости"
      type="2"
      cards={items}
      cardParams={{ hasFavorite: false }}
    />
  )
}

export default NewsSection
