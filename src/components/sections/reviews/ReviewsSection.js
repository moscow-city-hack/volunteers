import React, { useCallback, useMemo } from 'react'
import AvatarCard from 'components/cards/avatar/AvatarCard'
import Text from 'components/base/text/Text'
import Row from 'components/base/row/Row'
import Rating from 'components/base/rating/Rating'
import TitleSliderSection from 'components/sections/TitleSliderSection'
import './reviews.scss'

export type ReviewsItemType = {
  image?: string,
  description?: string,
  name?: string,
  stars: number,
}

export type ReviewsSectionProps = {
  items: Array<ReviewsItemType>,
}

const ReviewsSection = (props: ReviewsSectionProps) => {
  const { items = [] } = props

  const renderItem = useCallback(
    ({ image, description, name, stars, date }: ReviewsItemType, index: number) => (
      <AvatarCard
        key={index}
        index={index}
        avatar={image}
        avatarPlace="left"
        withShadow={false}
        description={description}
        renderAtEnd={() => (
          <div className="reviews__footer">
            <Text type="medium" variant="lg" className="reviews__name">
              {name}
            </Text>
            <Row horizontal="between" className="reviews__info">
              <Rating value={stars} />
              <Text variant="sm" className="reviews__date">
                {date}
              </Text>
            </Row>
          </div>
        )}
      />
    ),
    [],
  )

  const breakPoints = useMemo(
    () => ({ 1120: 2.5, 960: 2.25, 768: 1.75, 600: 1.25, 0: 1 }),
    [],
  )

  return (
    <TitleSliderSection
      items={items}
      title="Отзывы"
      className="reviews"
      breakPoints={breakPoints}
      renderItem={renderItem}
    />
  )
}

export default ReviewsSection
