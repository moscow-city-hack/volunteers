import React, { useMemo, useState, useRef, useCallback } from 'react'
import Section from 'components/sections/Section'
import BaseCard from 'components/cards/base/BaseCard'
import UserCard from 'components/cards/user/UserCard'
import SearchCard from 'components/cards/search/SearchCard'
import BasicMap from 'components/map/BasicMap'
import BaseSlider from 'components/slider/BaseSlider'
import Text from 'components/base/text/Text'
import Row from 'components/base/row/Row'
import SvgIcon from 'components/base/icon/SvgIcon'
import { ICONS } from 'assets/icons/icons'
import SideFilters, { FILTERS_TYPES } from 'components/filters/SideFilters'
import './search.scss'

export const VOLUNTEERS = 'volunteers'
export const ORGANISATIONS = 'organisations'
export const EVENTS = 'events'

export type SearchSectionProps = {
  place: VOLUNTEERS | ORGANISATIONS | EVENTS,
  data: Array<Record<string, any>>,
}

const SearchSection = (props: SearchSectionProps) => {
  const { place, data: items = [] } = props

  const data = items.slice(0, 20)

  const [isMapView, setIsMapView] = useState(false)
  const [current, setCurrent] = useState(null)

  const sliderRef = useRef(null)

  const volunteersFilters = useMemo(
    () => [
      FILTERS_TYPES.format,
      FILTERS_TYPES.conditions,
      FILTERS_TYPES.age,
      FILTERS_TYPES.roles,
      FILTERS_TYPES.skills,
      FILTERS_TYPES.work,
      FILTERS_TYPES.rating,
      FILTERS_TYPES.urgency,
    ],
    [],
  )

  const organisationsFilters = useMemo(
    () => [
      FILTERS_TYPES.organisation,
      FILTERS_TYPES.roles,
      FILTERS_TYPES.rating,
      FILTERS_TYPES.motivation,
    ],
    [],
  )

  const eventsFilters = useMemo(
    () => [FILTERS_TYPES.place, ...volunteersFilters, FILTERS_TYPES.motivation],
    [volunteersFilters],
  )

  const breakPoints = useMemo(
    () => ({
      0: { slidesPerView: 3, spaceBetween: 55 },
    }),
    [],
  )

  const onClickPlace = ({ id }, index) => {
    sliderRef.current?.slideTo(index)
    setCurrent(id)
  }

  const renderItem = useCallback(
    (user) => (
      <UserCard
        {...user}
        isActive={current === user.id}
        isCompact
        onClick={() => setCurrent(user.id)}
      />
    ),
    [current, setCurrent],
  )

  if (![VOLUNTEERS, ORGANISATIONS, EVENTS].includes(place)) {
    console.error(`Invalid search section location: ${place}`)
    return null
  }

  const isVolunters = place === VOLUNTEERS
  const isOrganisations = place === ORGANISATIONS

  const isCompactView = isVolunters || isOrganisations

  return (
    <Section className="search-section">
      <BaseCard renderContent={() => <SearchCard place={place} />} className="search" />
      <Row horizontal="between" vertical="top" className="search__content">
        <SideFilters
          filters={
            isVolunters
              ? volunteersFilters
              : isOrganisations
              ? organisationsFilters
              : eventsFilters
          }
        />
        {isCompactView ? (
          <div className="search__container">
            <Row
              className="search__watch"
              horizontal="right"
              onClick={() => setIsMapView((p) => !p)}
            >
              <Text variant="sm">
                {['Посмотреть', isMapView ? 'списком' : 'на карте'].join(' ')}
              </Text>
              <SvgIcon Icon={isMapView ? ICONS.list : ICONS.map} />
            </Row>
            {isMapView ? (
              <>
                <BaseSlider
                  ref={sliderRef}
                  items={data}
                  renderItem={renderItem}
                  breakPoints={breakPoints}
                  className="search__slider"
                />
                <BasicMap users={data} onClickPlace={onClickPlace} activeId={current} />
              </>
            ) : (
              <Row wrap className="search__users">
                {data.map((user, index) => (
                  <UserCard key={index} {...user} />
                ))}
              </Row>
            )}
          </div>
        ) : (
          <div className="search__cards">
            {data.map((card, index) => (
              <BaseCard key={index} {...card} hasFavorite={false} isImageFullHeight />
            ))}
          </div>
        )}
      </Row>
    </Section>
  )
}

export default SearchSection
