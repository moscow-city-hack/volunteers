import React from 'react'
import Section from 'components/sections/Section'
import Tabs from 'components/base/tabs/Tabs'
import './tabs.scss'

export type TabsSectionProps = {
  renderContent: Record<string, any>,
  tabs?: Array<Record<string, any>>,
}

const TabsSection = (props: TabsSectionProps) => {
  return (
    <Section className="tabs-section">
      <Tabs {...props} />
    </Section>
  )
}

export default TabsSection
