import React, { useCallback, ReactNode } from 'react'
import { Link } from 'react-router-dom'
import size from 'lodash/size'
import classNames from 'classnames'
import Section from 'components/sections/Section'
import Row from 'components/base/row/Row'
import Text from 'components/base/text/Text'
import Heading from 'components/base/text/Heading'
import BaseCard from 'components/cards/base/BaseCard'
import SvgIcon from 'components/base/icon/SvgIcon'
import { ICONS } from 'assets/icons/icons'
import type { HelpCardType } from 'components/sections/help/HelpSection'
import './titled-grid.scss'

export type TitledGridSectionProps = {
  className?: string,
  title?: string,
  cards?: Array<HelpCardType>,
  CardComponent?: string | ReactNode,
  cardParams?: Record<string, any>,
  type?: '1' | '2',
  hasWatchAll?: boolean,
  link?: string,
}

const TitledGridSection = (props: TitledGridSectionProps) => {
  const {
    className = '',
    title = '',
    cards = [],
    CardComponent = BaseCard,
    cardParams = {},
    type = '1',
    hasWatchAll = true,
    link = '',
  } = props

  const hasTitle = size(title) > 0

  const renderCard = useCallback(
    (card, index) => <CardComponent key={index} {...cardParams} {...card} />,
    [cardParams],
  )

  return (
    <Section className={classNames('titled-grid', className)}>
      <Row horizontal={hasTitle ? 'between' : 'right'}>
        <Heading text={title} />
        {hasWatchAll && (
          <Row Component={size(link) > 0 ? Link : 'div'} to={link} className="titled-grid__watch">
            <Text variant="lg">{'Смотреть все'}</Text>
            <SvgIcon Icon={ICONS.chevronRight} />
          </Row>
        )}
      </Row>
      {size(cards) > 0 && (
        <div className={classNames('titled-grid__grid', `grid-type-${type}`)}>
          {cards.map(renderCard)}
        </div>
      )}
    </Section>
  )
}

export default TitledGridSection
