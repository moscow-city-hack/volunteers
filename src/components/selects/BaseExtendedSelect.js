import React from 'react'
import ExtendedSelect, { formatOptionsForSelect } from 'components/selects/ExtendedSelect'
import type { ExtendedSelectProps } from 'components/selects/ExtendedSelect'

const BaseExtendedSelect = (props: Partial<ExtendedSelectProps>) => {
  const { defaultOptions, ...other } = props

  return (
    <ExtendedSelect
      {...other}
      isMulti
      allowRemoveItems
      allowCustomOption
      addCustomOptionOnBlur={false}
      allowEmptyValue
      noOptionsText="Ничего не найдено. Вы можете добавить свой вариант."
      defaultOptions={formatOptionsForSelect(defaultOptions)}
      clearOnBlur
    />
  )
}

export default BaseExtendedSelect
