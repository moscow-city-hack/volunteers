import React, {
  ReactNode,
  useCallback,
  useEffect,
  useMemo,
  useRef,
  useState,
} from 'react'
import autosize from 'autosize'
import classNames from 'classnames'
import size from 'lodash/size'
import { components as ReactSelectComponents } from 'react-select'
import Select, { doingSelectSearch } from 'components/selects/Select'
import Row from 'components/base/row/Row'
import HttpClient from 'network/HttpClient'
import type {
  SelectCallbackData,
  SelectOption,
  SelectProps,
} from 'components/selects/Select'
import type { HttpResponseError, HttpResponseSuccess } from 'network/HttpClient'

export type ExtendedSelectProps = {
  className?: string,
  serviceSearch?: string,
  allowRemoveItems?: boolean,
  allowCustomOption?: boolean,
  allowEditingValue?: boolean,
  addCustomOptionOnBlur?: boolean,
  value: SelectOption | string,
  params: { [key: string]: any },
  noOptionsText?: string,
  startSearchText?: string,
  components?: {},
  allowEmptyValue?: boolean,
  renderAtOptionEnd?: () => ReactNode,
  placeholder?: string,
  closeMenuAfterSelect?: boolean,
  hideMenuWhenEmpty?: boolean,
  clearOnBlur?: boolean,
} & $Shape<SelectProps>

export const formatOptionsForSelect = (data) => {
  return size(data) > 0
    ? data.map((i) => ({
        label: i.displayname,
        value: i.id,
        initialData: i,
      }))
    : []
}

const ExtendedSelect = (props: ExtendedSelectProps) => {
  const {
    className,
    id,
    value,
    serviceSearch,
    allowRemoveItems,
    allowCustomOption,
    allowEditingValue,
    addCustomOptionOnBlur,
    onChange,
    allowEmptyValue,
    params,
    isMulti,
    noOptionsText,
    defaultOptions,
    components,
    renderAtOptionEnd,
    startSearchText,
    isDisabled = false,
    closeMenuAfterSelect = false,
    hideMenuWhenEmpty = false,
    clearOnBlur = false,
    placeholder = '',
    ...other
  } = props

  const selectRef = useRef(null)
  const textAreaRef = useRef(null)
  const cancelSearch = useRef(null)
  const isAddingNewValue = useRef(false)

  const [inputValue, setInputValue] = useState('')
  const [menuIsOpen, setMenuIsOpen] = useState(false)
  const [options, setOptions] = useState([]) // Используется только для определения наличия найденных элементов

  const withTextArea = allowEditingValue

  const hasSearchValue = size(inputValue) > 2
  const activeOptions = hasSearchValue ? options : defaultOptions
  const hasOptions = size(activeOptions) > 0
  const isFocused = !isDisabled && menuIsOpen

  const fetchItems = useCallback(
    (search, callback) => {
      cancelSearch.current?.()
      cancelSearch.current = null

      HttpClient.request(
        {
          path: serviceSearch,
          getCancel: (cancel) => (cancelSearch.current = cancel),
        },
        { ...params, select: search },
      )
        .then((r: HttpResponseSuccess) => {
          const data = r.responseData
          const newOptions = formatOptionsForSelect(data)

          if (allowEditingValue && value) {
            const select = selectRef.current?.getSelect?.()?.select

            if (select) {
              select.setState({
                focusedOption: newOptions.find((i) => i.value === value.value),
              })
            }
          }

          callback(newOptions)
          setOptions(newOptions)
        })
        .catch((r: HttpResponseError) => {
          callback([])
          setOptions([])
        })
    },
    [serviceSearch, params, value, allowEditingValue, setOptions],
  )

  const mergeValueWithOption = useCallback(
    (option) => {
      const newOption = formatOptionsForSelect([option])[0]
      return {
        id,
        value: isMulti ? [...(Array.isArray(value) ? value : []), newOption] : newOption,
      }
    },
    [id, isMulti, value],
  )

  const _onChange = useCallback(
    (data: SelectCallbackData) => {
      if (data.action === 'select-option') {
        // Когда выбираем элемент вручную, то нужно избежать обработки значения при блюре.
        // Для этого сохраняем здесь флаг, а при блюре сбросим его
        isAddingNewValue.current = true

        // При выборе опции - очищаем инпут
        setInputValue('')

        !isMulti && selectRef.current?.blur()
        closeMenuAfterSelect && setMenuIsOpen(false)
      } else if (allowEditingValue && !isMulti && data.action === 'clear') {
        const inputText = value?.label || ''

        // Если длина равна 1, то значение будет очищено.
        // А если больше 1, то "стираем" часть его и помещаем в инпут поиска:
        if (size(inputText) > 1) {
          const value = inputText.slice(0, size(inputText) - 1)
          selectRef.current.setInputValue(value)

          return
        }
      }

      onChange(
        data?.value === null && allowEmptyValue
          ? { ...data, value: '' }
          : Array.isArray(data?.value) && size(data.value) === 0
          ? { ...data, value: null } // Зануляем массивное значение, чтобы было проще проходить проверку валидации в Yup
          : data,
      )
    },
    [onChange, isMulti, value, allowEditingValue, closeMenuAfterSelect, allowEmptyValue],
  )

  const addSearchItem = useCallback(() => {
    if (size(inputValue) && allowCustomOption) {
      onChange(mergeValueWithOption({ displayname: inputValue, isSearchItem: true }))
      setInputValue('')
    }
  }, [inputValue, allowCustomOption, mergeValueWithOption, onChange])

  const _onBlur = useCallback(() => {
    // Когда добавляем элемент, то не нужно обрабатывать значение в инпуте
    if (addCustomOptionOnBlur && !isAddingNewValue.current) {
      addSearchItem()
    } else if ((allowEditingValue || clearOnBlur) && size(inputValue) > 0) {
      // При блюре стираем значение в инпуте
      setInputValue('')
    }

    setMenuIsOpen(false)
    isAddingNewValue.current = false
  }, [addSearchItem, addCustomOptionOnBlur, allowEditingValue, clearOnBlur, inputValue])

  const onInputChange = (query, { action, isCustomAction }) => {
    if (action === 'input-change') {
      let result = query
      if (allowEditingValue && value?.value && !isCustomAction) {
        if (!query) {
          // Если был стерт последний символ, то очищаем текущее значение
          onChange({ value: null })
        } else if (!inputValue) {
          // Если не было значения, то выставляем в инпут текущее значение + новый символ
          result = value?.value + query
        }
      }

      setInputValue(result)
      return result
    }

    return inputValue
  }

  const onKeyDown = useCallback(
    ({ key }) => {
      if (key === 'Enter') {
        const activeItemClass = '.dropmenu__item.selected'
        const selectedItem =
          // Обрабатываем оба случая: меню в портале и без него
          document.querySelector(`.select__menu-portal ${activeItemClass}`) ||
          document.querySelector(`.select__menu ${activeItemClass}`)

        !selectedItem && addSearchItem()

        if (!isMulti && (!allowEditingValue || closeMenuAfterSelect)) {
          setMenuIsOpen(false)
        }
      } else {
        closeMenuAfterSelect && setMenuIsOpen(true)
      }
    },
    [addSearchItem, isMulti, allowEditingValue, closeMenuAfterSelect],
  )

  const renderItem = useCallback(
    (item: Record<string, any>) => {
      const { data, innerProps, isFocused, isSelected } = item.props

      return (
        <div
          className={classNames([
            'select__option',
            { added: isSelected, selected: isFocused },
          ])}
          {...innerProps}
        >
          <Row className="select__option__row">
            <Row className="select__option__row-content" wrap>
              <p className="select__option-text">{data.label}</p>
            </Row>
            <Row>{renderAtOptionEnd?.(data)}</Row>
          </Row>
        </div>
      )
    },
    [renderAtOptionEnd],
  )

  const _components = useMemo(() => {
    const result = {
      ClearIndicator: () => null,
      SingleValue: ({ data }) => {
        return <div className="select__single-value">{data.label || data.value}</div>
      },
      Option: (_props) => renderItem({ props: _props }),
    }
    if (withTextArea) {
      result['ValueContainer'] = ({ children, ..._props }) => {
        return (
          <div onMouseDown={() => textAreaRef.current?.focus()} style={{ flex: 1 }}>
            <ReactSelectComponents.ValueContainer {..._props}>
              {children}
            </ReactSelectComponents.ValueContainer>
          </div>
        )
      }
      result['Input'] = (_props) => {
        const hasValue = _props.value?.length > 0
        return (
          <div
            className={classNames('select__textarea-container', {
              'select__textarea-container--has-value': hasValue,
            })}
          >
            <textarea {..._props} ref={textAreaRef} className="select__textarea-input" />
          </div>
        )
      }
    }
    return result
  }, [withTextArea, renderItem])

  const selectComponents = useMemo(() => {
    return { ..._components, ...(components || {}) }
  }, [components, _components])

  useEffect(() => {
    if (withTextArea && textAreaRef.current) {
      autosize(textAreaRef.current)
    }
  }, [withTextArea])

  useEffect(() => {
    withTextArea && autosize.update(textAreaRef.current)
  }, [inputValue, withTextArea])

  return (
    <Select
      {...other}
      ref={selectRef}
      id={id}
      value={value}
      onChange={_onChange}
      defaultOptions={defaultOptions}
      components={selectComponents}
      loadOptions={serviceSearch ? fetchItems : undefined}
      isClearable={allowRemoveItems}
      escapeClearsValue={false}
      backspaceRemovesValue={allowRemoveItems}
      allowRemoveItems={allowRemoveItems}
      inputValue={inputValue}
      onInputChange={onInputChange}
      onBlur={_onBlur}
      onKeyDown={onKeyDown}
      menuIsOpen={hideMenuWhenEmpty ? isFocused && hasOptions : isFocused}
      isFocused={isFocused}
      onFocus={() => setMenuIsOpen(true)}
      isDisabled={isDisabled}
      placeholder={placeholder}
      isMulti={isMulti}
      noOptionsMessage={
        noOptionsText
          ? ({ inputValue }) => {
              return doingSelectSearch(inputValue) ||
                (size(defaultOptions) > 0 && size(inputValue) > 0) ? (
                <div dangerouslySetInnerHTML={{ __html: noOptionsText }} />
              ) : (
                'Начните поиск'
              )
            }
          : undefined
      }
    />
  )
}

export default ExtendedSelect
