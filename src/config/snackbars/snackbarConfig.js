/* eslint-disable react/destructuring-assignment */
import { OptionsObject, SnackbarMessage, useSnackbar, WithSnackbarProps } from 'notistack'
import React from 'react'

let useSnackbarRef: WithSnackbarProps

const setUseSnackbarRef = (useSnackbarRefProp: WithSnackbarProps) => {
  useSnackbarRef = useSnackbarRefProp
}

type InnerProps = {
  setUseSnackbarRef: (showSnackbar: WithSnackbarProps) => void,
}

const InnerSnackbarUtilsConfigurator = (props: InnerProps) => {
  props.setUseSnackbarRef(useSnackbar())
  return null
}

export const SnackbarUtilsConfigurator = () => {
  return <InnerSnackbarUtilsConfigurator setUseSnackbarRef={setUseSnackbarRef} />
}

/* eslint-disable */
export default {
  enqueueSnackbar(message: SnackbarMessage, options: OptionsObject = {}) {
    useSnackbarRef.enqueueSnackbar(message, {
      ...options,
      action: (key) => (<>{options.action?.(key)}</>),
    })
  },
  closeSnackbar(key) {
    useSnackbarRef.closeSnackbar(key)
  },
}
