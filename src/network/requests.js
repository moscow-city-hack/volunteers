import HttpClient from 'network/HttpClient'
import { API_PATHS } from 'network/apiPaths'
import type { HttpResponseSuccess, HttpResponseError } from 'network/HttpClient'
import type { UniversalRequestParams } from 'network/types'

const universalRequest = (
  { onError, onFinally, errorMessage }: UniversalRequestParams = {},
  requestParams: Record<string, any> = {},
  params: Record<string, any> = {},
) => {
  return HttpClient.request(requestParams, {}, params)
    .then(({ responseData }: HttpResponseSuccess) => responseData)
    .catch(({ message }: HttpResponseError) => {
      console.log(errorMessage ?? `Ошибка при получении данных: ${message}`)
      alert('Произошла ошибка при получении данных')
      onError?.(message)
    })
    .finally(() => onFinally?.())
}

export const getHome = () => {
  return universalRequest(undefined, { path: API_PATHS.GET_HOME, method: 'GET' })
}

export const getVolunteers = () => {
  return universalRequest(undefined, { path: API_PATHS.GET_VOLUNTEERS, method: 'GET' })
}

export const getOrganisations = () => {
  return universalRequest(undefined, { path: API_PATHS.GET_ORGANISATIONS, method: 'GET' })
}

export const getGoodWorks = () => {
  return universalRequest(undefined, { path: API_PATHS.GET_GOOD_WORKS, method: 'GET' })
}

export const login = (params: Record<string, any>) => {
  return universalRequest(undefined, { path: API_PATHS.LOGIN, method: 'GET' }, params)
}

export const logout = () => {
  return universalRequest(undefined, { path: API_PATHS.LOGOUT, method: 'GET' })
}

export const registration = (params: Record<string, any>) => {
  return universalRequest(
    undefined,
    { path: API_PATHS.REGISTRATION, method: 'POST' },
    params,
  )
}

export const getFavoritesEvents = () => {
  return universalRequest(undefined, {
    path: API_PATHS.GET_FAVORITES_GOOD_WORKS,
    method: 'GET',
  })
}

export const getMyEvents = () => {
  return universalRequest(undefined, { path: API_PATHS.GET_MY_GOOD_WORKS, method: 'GET' })
}
