export type UniversalRequestParams = {
  onError?: (error: string) => void,
  onFinally?: () => void,
  errorMessage?: string,
}

export type FetchConfigType = {
  params?: Record<string, any>,
  queryOptions?: Record<string, any>,
  dependencies?: Record<string, any>,
}
