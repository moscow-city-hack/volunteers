import React, { useState } from 'react'
import Header from 'components/header/Header'
import FooterSection from 'components/sections/footer/FooterSection'
import SearchSection, { EVENTS } from 'components/sections/search/SearchSection'
import LoadedContent from 'components/base/loaded/LoadedContent'
import { getGoodWorks } from 'network/requests'
import { useFetch } from 'utils/hooks'

const GoodWorks = () => {
  const [data, setData] = useState({})

  const { isLoading } = useFetch(getGoodWorks, 'getGoodWorks', {
    queryOptions: {
      onSuccess: (data) => setData(data),
    },
  })

  return (
    <LoadedContent
      isLoading={isLoading}
      data={data}
      renderContent={() => (
        <>
          <Header active={EVENTS} />
          <SearchSection place={EVENTS} data={data} />
          <FooterSection />
        </>
      )}
    />
  )
}

export default GoodWorks
