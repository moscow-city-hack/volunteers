import React, { useState } from 'react'
import Header from 'components/header/Header'
import HeadSection from 'components/sections/head/HeadSection'
import HelpSection from 'components/sections/help/HelpSection'
import NewsSection from 'components/sections/news/NewsSection'
import AdvantagesSection from 'components/sections/advantages/AdvantagesSection'
import ReviewsSection from 'components/sections/reviews/ReviewsSection'
import FooterSection from 'components/sections/footer/FooterSection'
import LoadedContent from 'components/base/loaded/LoadedContent'
import { getHome } from 'network/requests'
import { useFetch } from 'utils/hooks'

const Home = () => {
  const [data, setData] = useState({})

  const { isLoading } = useFetch(getHome, 'getHome', {
    queryOptions: {
      onSuccess: (data) => setData(data),
    },
  })

  const { advantages = [], events = [], news = [], reviews = [] } = data

  return (
    <LoadedContent
      isLoading={isLoading}
      data={data}
      renderContent={() => (
        <>
          <Header />
          <HeadSection />
          <AdvantagesSection items={advantages} />
          <HelpSection items={events} />
          <NewsSection items={news} />
          <ReviewsSection items={reviews} />
          <FooterSection />
        </>
      )}
    />
  )
}

export default Home
