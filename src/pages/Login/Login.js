import React, { useState, useCallback } from 'react'
import { Link } from 'react-router-dom'
import { connect } from 'react-redux'
import BaseCard from 'components/cards/base/BaseCard'
import Heading from 'components/base/text/Heading'
import Button from 'components/base/button/Button'
import Text from 'components/base/text/Text'
import Input from 'components/inputs/input/Input'
import Row from 'components/base/row/Row'
import logo from 'assets/images/logo.png'
import { login } from 'network/requests'
import { setLogged, setData } from 'store/Settings/actions'
import type { InputCallbackData } from 'components/inputs/input/Input'
import './login.scss'

const Login = (props) => {
  const { setLogged, setData } = props

  const [email, setEmail] = useState('')
  const [password, setPassword] = useState('')
  const [isFetching, setIsFetching] = useState(false)

  const onChangeEmail = useCallback(
    ({ value }: InputCallbackData) => setEmail(value),
    [setEmail],
  )

  const onChangePassword = useCallback(
    ({ value }: InputCallbackData) => setPassword(value),
    [setPassword],
  )

  const onResetEmail = useCallback(() => setEmail(''), [setEmail])
  const onResetPassword = useCallback(() => setPassword(''), [setPassword])

  const onLogin = useCallback(() => {
    setIsFetching(true)
    login({ email, password }).then(({ isLoggedIn, user }) => {
      setLogged(isLoggedIn)
      setData(user)
      setIsFetching(false)
    })
  }, [email, password, setIsFetching, setLogged, setData])

  const isDisabled = [email, password].includes('')

  const Component = isDisabled ? 'div' : Link

  return (
    <div className="login-wrapper">
      <BaseCard
        className="login"
        renderContent={() => (
          <>
            <Row horizontal="between" className="login__header">
              <Heading text="Вход" withSpacing={false} />
              <Link to="/">
                <img src={logo} alt="" />
              </Link>
            </Row>
            <Input
              type="text"
              val={email}
              onChange={onChangeEmail}
              onReset={onResetEmail}
              placeholder="Электронная почта"
              className="login__email"
            />
            <Input
              type="password"
              val={password}
              onChange={onChangePassword}
              onReset={onResetPassword}
              placeholder="Пароль"
              className="login__password"
            />
            <Text className="login__text">{'Забыли пароль?'}</Text>
            <Component to="/" onClick={onLogin}>
              <Button
                isFetching={!isDisabled && isFetching}
                disabled={isDisabled}
                text="Войти"
                stretch
                className="login__button"
              />
            </Component>
          </>
        )}
      />
    </div>
  )
}

const mapActionsToProps = {
  setLogged,
  setData,
}

export default connect(null, mapActionsToProps)(Login)
