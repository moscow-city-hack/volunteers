import React, { useState } from 'react'
import Header from 'components/header/Header'
import FooterSection from 'components/sections/footer/FooterSection'
import SearchSection, { ORGANISATIONS } from 'components/sections/search/SearchSection'
import LoadedContent from 'components/base/loaded/LoadedContent'
import { getOrganisations } from 'network/requests'
import { useFetch } from 'utils/hooks'

const Organisations = () => {
  const [data, setData] = useState({})

  const { isLoading } = useFetch(getOrganisations, 'getOrganisations', {
    queryOptions: {
      onSuccess: (data) => setData(data),
    },
  })

  return (
    <LoadedContent
      isLoading={isLoading}
      data={data}
      renderContent={() => (
        <>
          <Header active={ORGANISATIONS} />
          <SearchSection place={ORGANISATIONS} data={data} />
          <FooterSection />
        </>
      )}
    />
  )
}

export default Organisations
