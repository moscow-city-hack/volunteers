import React, { useMemo, useState, useCallback } from 'react'
import { connect } from 'react-redux'
import Row from 'components/base/row/Row'
import Button from 'components/base/button/Button'
import BaseCard from 'components/cards/base/BaseCard'
import Header from 'components/header/Header'
import Preloader from 'components/base/preloader/Preloader'
import TabsSection from 'components/sections/tabs/TabsSection'
import FooterSection from 'components/sections/footer/FooterSection'
import AboutCard from 'components/cards/about/AboutCard'
import AchievementsCard from 'components/cards/achievements/AchievementsCard'
import BadgesCard from 'components/cards/badge/BadgesCard'
import AvatarBlock from 'components/base/avatar/AvatarBlock'
import { SettingsSelectors } from 'store/Settings/reducers'
import { ICONS } from 'assets/icons/icons'
import { getMyEvents, getFavoritesEvents } from 'network/requests'
import { useFetch } from 'utils/hooks'
import './cabinet.scss'

const PersonalCabinet = (props) => {
  const { data = {} } = props

  const [myEvents, setMyEvents] = useState([])
  const [favoritesEvents, setFavoritesEvents] = useState([])

  const { isMyLoading } = useFetch(getMyEvents, 'getMyEvents', {
    queryOptions: {
      onSuccess: (data) => setMyEvents(data),
    },
  })

  const { isFavoritesLoading } = useFetch(getFavoritesEvents, 'getFavoritesEvents', {
    queryOptions: {
      onSuccess: (data) => setFavoritesEvents(data),
    },
  })

  const isLoading = isMyLoading || isFavoritesLoading

  const tabs = useMemo(
    () => [
      { text: 'Мои данные', id: 'data', icon: ICONS.user },
      { text: 'Мои заявки', id: 'applications', icon: ICONS.doc },
      { text: 'Организации', id: 'organisations', icon: ICONS.home },
      { text: 'Избранное', id: 'favorite', icon: ICONS.heart },
    ],
    [],
  )

  const badges = useMemo(
    () => [
      { count: 39, text: 'Мероприятий' },
      { count: 146, text: 'Часов работы' },
    ],
    [],
  )

  const achievements = useMemo(
    () => ['Волонтер|недели', 'Помощь|другу', 'Любитель|леса'],
    [],
  )

  const categories = useMemo(
    () => [
      'Животные',
      'Поиск пропавших',
      'Помощь природе',
      'Образование',
      'Помщь пожилым',
    ],
    [],
  )

  const renderEvents = useCallback(
    (items: Array<Record<string, any>>, hasFavorite: boolean = false) => {
      return (
        <div className="search__cards">
          {items.map((card, index) => (
            <BaseCard key={index} {...card} hasFavorite={hasFavorite} />
          ))}
        </div>
      )
    },
    [],
  )

  const renderContent = useMemo(
    () => ({
      data: (
        <>
          <Row className="cabinet__row" horizontal="between">
            <AboutCard data={data} badges={badges} />
            <AchievementsCard achievements={achievements} />
          </Row>
          <Row className="cabinet__row">
            <BadgesCard
              badges={data.skills?.map(({ displayname }) => displayname) ?? []}
              title="Навыки"
            />
            <BadgesCard badges={categories} title="Категории" />
          </Row>
        </>
      ),
      applications: renderEvents(myEvents),
      organisations: (
        <div className="search__cards">
          {Array.from(Array(8).keys()).map((_, index) => (
            <BaseCard
              key={index}
              className="cabinet__organisation"
              renderContent={() => (
                <>
                  <AvatarBlock
                    {...{
                      ...data,
                      name: `ООО Организация ${index + 1}`,
                      image:
                        'https://upload.wikimedia.org/wikipedia/commons/d/db/Zeronet_logo.png',
                    }}
                  />
                  <Row className="cabinet__organisation-buttons">
                    <Button text="Написать" size="small" stretch />
                    <Button text="Уволиться" size="small" variant="outline" stretch />
                  </Row>
                </>
              )}
            />
          ))}
        </div>
      ),
      favorite: renderEvents(
        favoritesEvents.map((el) => ({ ...el, favorite: true })),
        true,
      ),
    }),
    [data, badges, achievements, categories, renderEvents, myEvents, favoritesEvents],
  )

  return isLoading ? (
    <Preloader size={80} />
  ) : (
    <div className="cabinet">
      <Header />
      <TabsSection tabs={tabs} renderContent={renderContent} />
      <FooterSection />
    </div>
  )
}

const mapStateToProps = (state) => ({
  data: SettingsSelectors.getData(state),
})

export default connect(mapStateToProps)(PersonalCabinet)
