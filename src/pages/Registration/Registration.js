import React, { useState, useCallback } from 'react'
import { Link } from 'react-router-dom'
import { connect } from 'react-redux'
import classNames from 'classnames'
import BaseCard from 'components/cards/base/BaseCard'
import Heading from 'components/base/text/Heading'
import Text from 'components/base/text/Text'
import Button from 'components/base/button/Button'
import Input from 'components/inputs/input/Input'
import Row from 'components/base/row/Row'
import logo from 'assets/images/logo.png'
import { registration } from 'network/requests'
import { setLogged, setData } from 'store/Settings/actions'
import type { InputCallbackData } from 'components/inputs/input/Input'
import './registration.scss'

export const INDIVIDUAL = 'individual'
export const ORGANISATION = 'organisation'

const Registration = (props) => {
  const { setLogged, setData } = props

  const [type, setType] = useState(INDIVIDUAL)
  const [name, setName] = useState('')
  const [surname, setSurname] = useState('')
  const [birthday, setBirthday] = useState('')
  const [email, setEmail] = useState('')
  const [phone, setPhone] = useState('')
  const [password, setPassword] = useState('')
  const [repeatPassword, setRepeatPassword] = useState('')
  const [isFetching, setIsFetching] = useState(false)

  const onChangeName = useCallback(
    ({ value }: InputCallbackData) => setName(value),
    [setName],
  )

  const onChangeSurname = useCallback(
    ({ value }: InputCallbackData) => setSurname(value),
    [setSurname],
  )

  const onChangeBirthday = useCallback(
    ({ value }: InputCallbackData) => setBirthday(value),
    [setBirthday],
  )

  const onChangeEmail = useCallback(
    ({ value }: InputCallbackData) => setEmail(value),
    [setEmail],
  )

  const onChangePhone = useCallback(
    ({ value }: InputCallbackData) => setPhone(value),
    [setPhone],
  )

  const onChangePassword = useCallback(
    ({ value }: InputCallbackData) => setPassword(value),
    [setPassword],
  )

  const onChangeRepeatPassword = useCallback(
    ({ value }: InputCallbackData) => setRepeatPassword(value),
    [setRepeatPassword],
  )

  const onResetName = useCallback(() => setName(''), [setName])
  const onResetSurname = useCallback(() => setSurname(''), [setSurname])
  const onResetBirthday = useCallback(() => setBirthday(''), [setBirthday])
  const onResetEmail = useCallback(() => setEmail(''), [setEmail])
  const onResetPhone = useCallback(() => setPhone(''), [setPhone])
  const onResetPassword = useCallback(() => setPassword(''), [setPassword])
  const onResetRepeat = useCallback(() => setRepeatPassword(''), [setRepeatPassword])

  const onRegistration = useCallback(() => {
    setIsFetching(true)
    registration({
      name,
      surname,
      birthday,
      email,
      phone,
      password,
      repeatPassword,
      type,
    }).then(({ isLoggedIn, user }) => {
      setLogged(isLoggedIn)
      setData(user)
      setIsFetching(false)
    })
  }, [
    name,
    surname,
    birthday,
    email,
    phone,
    password,
    repeatPassword,
    type,
    setIsFetching,
    setLogged,
    setData,
  ])

  const isDisabled = [
    name,
    surname,
    birthday,
    email,
    phone,
    password,
    repeatPassword,
  ].includes('')

  const Component = isDisabled ? 'div' : Link

  const renderTextBlock = useCallback(
    (key: string, text: string) => {
      const active = type === key

      return (
        <Text
          className={classNames('registration__selector-text', { active })}
          onClick={() => !active && setType(key)}
        >
          {text}
        </Text>
      )
    },
    [type],
  )

  return (
    <div className="registration-wrapper">
      <BaseCard
        className="registration"
        renderContent={() => (
          <>
            <Row horizontal="between" className="registration__header">
              <Heading text="Регистрация" withSpacing={false} />
              <Link to="/">
                <img src={logo} alt="" />
              </Link>
            </Row>
            <Row className="registration__selector">
              {renderTextBlock(INDIVIDUAL, 'Физическое лицо')}
              {renderTextBlock(ORGANISATION, 'Организация')}
            </Row>
            <Input
              type="text"
              val={name}
              onChange={onChangeName}
              onReset={onResetName}
              placeholder="Имя"
              className="registration__name"
            />
            <Input
              type="text"
              val={surname}
              onChange={onChangeSurname}
              onReset={onResetSurname}
              placeholder="Фамилия"
              className="registration__surname"
            />
            <Input
              type="date"
              val={birthday}
              onChange={onChangeBirthday}
              onReset={onResetBirthday}
              placeholder="Дата рождения"
              className="registration__birthday"
              hasDisabledDays={false}
              datePickerMinDate="01.01.1970"
            />
            <Input
              type="text"
              val={email}
              onChange={onChangeEmail}
              onReset={onResetEmail}
              placeholder="Электронная почта"
              className="registration__email"
            />
            <Input
              type="text"
              val={phone}
              onChange={onChangePhone}
              onReset={onResetPhone}
              placeholder="Номер телефона"
              className="registration__phone"
              format="+7 (xxx) xxx-xx-xx"
            />
            <Input
              type="password"
              val={password}
              onChange={onChangePassword}
              onReset={onResetPassword}
              placeholder="Пароль"
              className="registration__password"
            />
            <Input
              type="password"
              val={repeatPassword}
              onChange={onChangeRepeatPassword}
              onReset={onResetRepeat}
              placeholder="Повторный пароль"
              className="registration__repeat-password"
            />
            <Component to="/" onClick={onRegistration}>
              <Button
                isFetching={!isDisabled && isFetching}
                disabled={isDisabled}
                text="Зарегистрироваться"
                stretch
                className="registration__button"
              />
            </Component>
          </>
        )}
      />
    </div>
  )
}

const mapActionsToProps = {
  setLogged,
  setData,
}

export default connect(null, mapActionsToProps)(Registration)
