import React, { useState } from 'react'
import Header from 'components/header/Header'
import FooterSection from 'components/sections/footer/FooterSection'
import SearchSection, { VOLUNTEERS } from 'components/sections/search/SearchSection'
import LoadedContent from 'components/base/loaded/LoadedContent'
import { getVolunteers } from 'network/requests'
import { useFetch } from 'utils/hooks'

const Volunteers = () => {
  const [data, setData] = useState({})

  const { isLoading } = useFetch(getVolunteers, 'getVolunteers', {
    queryOptions: {
      onSuccess: (data) => setData(data),
    },
  })

  return (
    <LoadedContent
      isLoading={isLoading}
      data={data}
      renderContent={() => (
        <>
          <Header active={VOLUNTEERS} />
          <SearchSection place={VOLUNTEERS} data={data} />
          <FooterSection />
        </>
      )}
    />
  )
}

export default Volunteers
