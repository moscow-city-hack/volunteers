export const SET_LOGGED = 'SET_LOGGED'
export const SET_DATA = 'SET_DATA'

export const setLogged = (isLoggedIn: boolean) => ({
  type: SET_LOGGED,
  isLoggedIn,
})

export const setData = (data: Record<string, any>) => ({
  type: SET_DATA,
  data,
})
