import { SET_LOGGED, SET_DATA } from './actions'

const initialState = {
  isLoggedIn: false,
}

export const settingsReducer = (state = { ...initialState }, action) => {
  switch (action.type) {
    case SET_LOGGED:
      return { ...state, isLoggedIn: action.isLoggedIn }
    case SET_DATA:
      return { ...state, data: action.data }
    default:
      return state
  }
}

export const SettingsSelectors = {
  getIsLogged: ({ settings }) => settings.isLoggedIn,
  getData: ({ settings }) => settings.data,
}
