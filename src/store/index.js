import { routerMiddleware } from 'connected-react-router'
import Thunk from 'redux-thunk'
import { createLogger } from 'redux-logger'
import { applyMiddleware, createStore } from 'redux'
import { connectRouter } from 'connected-react-router'
import { createBrowserHistory } from 'history'
import { composeWithDevTools } from 'redux-devtools-extension/logOnlyInProduction'
import { combineReducers } from 'redux'
import { settingsReducer } from 'store/Settings/reducers'

export const history = createBrowserHistory({ basename: '/' })

export const rootReducer = combineReducers({
  settings: settingsReducer,
  router: connectRouter(history),
})

const configureStore = (initialState = {}) => {
  const middleWares = [routerMiddleware(history), Thunk]

  if (process.env.NODE_ENV !== 'production') {
    middleWares.push(createLogger({ collapsed: true }))
  }

  return createStore(
    rootReducer,
    initialState,
    composeWithDevTools(applyMiddleware(...middleWares)),
  )
}

const store = configureStore()
window.store = store

export type RootState = ReturnType<typeof store.getState>
export default store
