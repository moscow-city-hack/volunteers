import { Ref } from 'react'
import ReactDom from 'react-dom'
import isString from 'lodash/isString'
import padStart from 'lodash/padStart'
import get from 'lodash/get'
import trim from 'lodash/trim'
import size from 'lodash/size'
import isFinite from 'lodash/isFinite'
import isFunction from 'lodash/isFunction'
import Qs from 'qs'

export const INVISIBLE_TEXT = '‎'
export const SPECIAL_SYMBOLS = ' !#$%&\'()*+,-./:;<=>?@[\\]^_{|}~`"'

export const stopPropagationEvent = (event: MouseEvent<HTMLDivElement>) => {
  event?.stopPropagation()
}

export const removeSymbols = (str: string, symbols: Array<string>) => {
  if (!isString(str) || !Array.isArray(symbols)) {
    return ''
  }

  if (size(symbols) === 0) {
    return str
  }

  return removeSymbols(replaceAll(str, symbols[0], ''), symbols.slice(1))
}

export const getQueryParams = () => parseQuery(window.location.search)

export const parseQuery = (queryString: string) => {
  const query = {}
  const str = queryString[0] === '?' ? queryString.substr(1) : queryString
  const pairs = str.split('&')

  for (let i = 0; i < size(pairs); i++) {
    const pair = pairs[i].split('=')
    query[decodeURIComponent(pair[0])] = decodeURIComponent(pair[1] || '')
  }

  return query
}

export const parseQueryParams = (queryString: string): Record<string, string> => {
  return Qs.parse(queryString, { ignoreQueryPrefix: true })
}

export const getQueryParam = (queryString: string, paramName: string): string => {
  const params = parseQueryParams(queryString)
  return params?.[paramName]
}

export const replaceLast = (
  str: string,
  pattern: string | RegExp,
  replacement: string,
) => {
  const match =
    typeof pattern === 'string'
      ? pattern
      : (str.match(new RegExp(pattern.source, 'g')) || []).slice(-1)[0]

  if (!match) {
    return str
  }

  const lastIndex = str.lastIndexOf(match)

  return lastIndex !== -1
    ? [str.slice(0, lastIndex), str.slice(lastIndex + match.length)].join(replacement)
    : str
}

export const copyToClipboard = (text: string, hasDelay: boolean = false) => {
  const copy = () => {
    const tempElement = document.createElement('textarea') // Create a <textarea> element

    tempElement.value = text // Set its value to the string that you want copied
    tempElement.setAttribute('readonly', '') // Make it readonly to be tamper-proof
    tempElement.classList.add('visually-hidden')

    document.body.appendChild(tempElement) // Append the <textarea> element to the HTML document

    const selected =
      document.getSelection().rangeCount > 0 // Check if there is any content selected previously
        ? document.getSelection().getRangeAt(0) // Store selection if found
        : false // Mark as false to know no selection existed before

    tempElement.select() // Select the <textarea> content

    document.execCommand('copy') // Copy - only works as a result of a user action (e.g. click events)
    document.body.removeChild(tempElement) // Remove the <textarea> element

    if (selected) {
      // If a selection existed before copying
      document.getSelection().removeAllRanges() // Unselect everything on the HTML document
      document.getSelection().addRange(selected) // Restore the original selection
    }
  }

  if (navigator.clipboard) {
    navigator.clipboard.writeText(text).then(() => {
      // clipboard successfully set
      // (works only with https protocol and when DOM is focused)
    })
  } else {
    hasDelay ? wait(300).then(() => copy()) : copy()
  }
}

export const wait = (ms: number = 1000) => {
  return new Promise((resolve) => setTimeout(resolve, ms))
}

export const promiseWithDelay = (n: number) => {
  return new Promise((resolve) => setTimeout(resolve, n))
}

export const getElementRectByRef = (ref: Ref) => {
  const element = ReactDom.findDOMNode(ref.current)
  return element ? element.getBoundingClientRect() : {}
}

export const getSelectIndexOptions = (count: number, padLength: number = 0) => {
  return Array.from(Array(count).keys()).map((v) => ({
    id: v + 1,
    title: padLength ? padStart(v + 1, padLength, '0') : v + 1,
  }))
}

export type CustomEventFn<T> = (event: Event & { _customData: T }) => void

export const dispatchCustomEvent = (name: string, customData?: any) => {
  let event

  if (isFunction(Event)) {
    event = new Event(name)
  } else {
    /* IE */
    event = document.createEvent('Event')
    event.initEvent(name, true, true)
  }

  event._customData = customData
  window.dispatchEvent(event)
}

export const getScrollWidth = () => {
  const div = document.createElement('div')

  div.style.overflowY = 'scroll'
  div.style.width = '50px'
  div.style.height = '50px'
  div.style.visibility = 'hidden'

  document.body.appendChild(div)
  const scrollWidth = div.offsetWidth - div.clientWidth
  document.body.removeChild(div)

  return scrollWidth
}

export type ScrollHandlerParams = {
  onStart: () => void,
  onStop: () => void,
  timeout: number,
}

export const createOnScrollHandler = (params: ScrollHandlerParams) => {
  const { onStart, onStop, timeout = 200 } = params
  let timer = null

  return (event) => {
    timer ? clearTimeout(timer) : onStart?.(event)

    timer = setTimeout(() => {
      timer = null
      onStop?.(event)
    }, timeout)
  }
}

export const createScrollDirectionHandler = () => {
  let prevTop = 0
  let prevLeft = 0

  return {
    // Используется для актуализации данных (например, при использовании разных обработчиков - onStart + onStop)
    updateOnScroll: (event: MouseEvent<HTMLDivElement>) => {
      const { target } = event
      const { scrollTop, scrollLeft } = target
      prevTop = scrollTop
      prevLeft = scrollLeft
    },
    getDirection: (
      event: MouseEvent<HTMLDivElement>,
    ): 'down' | 'up' | 'right' | 'left' | 'static' => {
      const { target } = event
      let direction = 'static'
      const { scrollTop, scrollLeft } = target

      if (scrollTop > prevTop) {
        prevTop = scrollTop
        direction = 'down'
      } else if (scrollTop < prevTop) {
        prevTop = scrollTop
        direction = 'up'
      } else if (scrollLeft > prevLeft) {
        prevLeft = scrollLeft
        direction = 'right'
      } else if (scrollLeft < prevLeft) {
        prevLeft = scrollLeft
        direction = 'left'
      }

      return direction
    },
  }
}

export const getNoun = (
  value: number,
  strings: { base: string, one: string, two: string },
) => {
  const { one, two, base } = strings

  let n = Math.abs(value)
  n %= 100

  if (n >= 5 && n <= 20) {
    return base
  }

  n %= 10
  if (n === 1) {
    return one
  }

  if (n >= 2 && n <= 4) {
    return two
  }

  return base
}

export const classListContains = (
  event: MouseEvent<HTMLDivElement>,
  className: string,
) => {
  const classList = get(event, ['target', 'classList'])

  for (const key in classList) {
    const cn = classList[key]

    if (isString(cn) && cn.includes(className)) {
      return true
    }
  }

  return false
}

const keysMatching = {
  q: 'й',
  w: 'ц',
  e: 'у',
  r: 'к',
  t: 'е',
  y: 'н',
  u: 'г',
  i: 'ш',
  o: 'щ',
  p: 'з',
  '[': 'х',
  ']': 'ъ',
  a: 'ф',
  s: 'ы',
  d: 'в',
  f: 'а',
  g: 'п',
  h: 'р',
  j: 'о',
  k: 'л',
  l: 'д',
  ';': 'ж',
  "'": 'э',
  z: 'я',
  x: 'ч',
  c: 'с',
  v: 'м',
  b: 'и',
  n: 'т',
  m: 'ь',
  ',': 'б',
  '.': 'ю',
  '/': '.',
}

export const keysMatchingInverse = Object.keys(keysMatching).reduce((acc, engChar) => {
  acc[keysMatching[engChar]] = engChar
  return acc
}, {})

export const replaceWordLetters = (
  word: string,
  replacer: (letter: string) => string,
) => {
  return word.split('').map(replacer).join('')
}

export const inverseWord = (word: string) => {
  return replaceWordLetters(word, (l) => {
    return keysMatching[l] || keysMatchingInverse[l] || l
  })
}

export const isMultiKeySearchMatches = (search: string, source: string) => {
  search = trim(search)
  source = trim(source)

  if (!search) {
    return true
  }

  if (!source) {
    return false
  }

  source = source.toLowerCase()
  search = search.toLowerCase()

  return (
    source.includes(search) ||
    inverseWord(source).includes(search) ||
    source.includes(inverseWord(search))
  )
}

/* eslint-disable no-useless-escape */
const EmailRegExp =
  /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/

export const isEmail = (email: string): boolean => {
  return isString(email) ? EmailRegExp.test(email.toLowerCase()) : false
}

export const toFixedN = (
  number: number,
  fractionDigits: number = 8,
  canBeNull: boolean = true,
): string => {
  let num = number

  num = +num
  if (isNaN(num)) {
    return '-'
  }

  if (num === 0) {
    return canBeNull ? '0' : '~0'
  }

  if (num < 1) {
    // Нельзя использовать parseFloat для маленьких значений из-за экспоненциальной формы
    num = num.toFixed(fractionDigits).replace(/\.?0+$/, '')
  } else {
    num = parseFloat(num.toFixed(fractionDigits))
  }

  return +num === 0 ? `~${num}` : num.toString()
}

export const toArrayIfNot = (value: any) => (Array.isArray(value) ? value : [value])

export const getHtmlTagAttribute = (name?: string): string | null => {
  const html = document.documentElement
  return html?.dataset?.[name] || null
}

export const replaceAll = (str: string, find: string, replacement: string) => {
  return str?.split(find)?.join(replacement)
}

export const getElementSize = (elem: HTMLDivElement) => {
  if (elem) {
    const elemStyle = window.getComputedStyle(elem)

    const height = +removeSymbols(elemStyle['height'], ['px'])
    const width = +removeSymbols(elemStyle['width'], ['px'])

    return {
      height: isFinite(height) ? height : 0,
      width: isFinite(width) ? width : 0,
    }
  }

  return { height: 0, width: 0 }
}

export const clickTrigger = (selector: string) => {
  document.querySelector(selector)?.dispatchEvent(
    new MouseEvent('click', {
      view: window,
      bubbles: true,
      cancelable: true,
    }),
  )
}

export const removeClassBySelector = (selector: string, className: string) => {
  document.querySelectorAll(selector).forEach((node) => {
    node?.classList.remove(className)
  })
}

export const getMaskFromFormat = (format: string) => {
  return replaceAll(replaceAll(format, '9', '\\9'), 'x', '9')
}

const nounsById = {
  days: { base: 'дней', one: 'день', two: 'дня' },
  months: { base: 'месяцев', one: 'месяц', two: 'месяца' },
  minutes: { base: 'минут', one: 'минута', two: 'минуты' },
}

export const getNounByKey = (
  key: $Keys<typeof nounsById>,
  count: number,
  renderCount: boolean,
) => {
  const strings = nounsById[key]

  if (strings) {
    return `${renderCount ? `${count} ` : ''}${getNoun(count, strings)}`
  }

  console.warn(`Not found noun with key ${key}`, nounsById)
  return renderCount ? count : ''
}

export const replaceWithHtml = (
  text: string,
  textToReplace: string,
  replaceValue: any,
  replaceAllEntries: boolean = false,
): any[] => {
  const delimiter = '|||||'
  const newTextToReplace = `${delimiter}${textToReplace}${delimiter}`
  return text?.includes(textToReplace)
    ? (replaceAllEntries
        ? replaceAll(text, textToReplace, newTextToReplace)
        : text.replace(textToReplace, newTextToReplace)
      )
        .split(delimiter)
        .map((i) => (i === textToReplace ? replaceValue : i || null))
    : text
}

export const replaceLineBreaksWithHtml = (text: string) => {
  return replaceWithHtml(replaceAll(text, '\n', '/n'), '/n', <br />, true)
}

export const sortByAlphabet = (array: Array<Record<string, any>>, sortKey: string) => {
  return array
    .slice(0)
    .sort((a, b) =>
      get(a, sortKey)?.toLowerCase().localeCompare(get(b, sortKey)?.toLowerCase()),
    )
}

export const getModifiers = (
  prefix: string,
  modifiers: string | Array<string>,
  divider: string = '--',
) => {
  return [prefix, ...toArrayIfNot(modifiers)].filter(Boolean).join(divider)
}

export const format = (value: number) => padStart(value.toString(), 2, '0')
