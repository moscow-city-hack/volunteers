import get from 'lodash/get'
import pickBy from 'lodash/pickBy'
import { FormikHelpers, FormikProps, FormikValues, FormikErrors } from 'formik'
import type { HttpResponseError } from 'network/HttpClient'

export const API_ERRORS_KEY = 'api_errors'

export type FormikType = FormikProps<FormikValues>
export type FormikHelpersType = FormikHelpers<FormikValues>
export type FormErrors = FormikErrors<FormikValues>
export type InitialValuesType = Record<string, any>
export type ValidationSchemeType = Record<any, any>
export type ValidateForm = (values?: any) => Promise<FormErrors>
export type ErrorsKeyModifierType = (key: string, location: string[]) => string
export type FormikOnSubmit = (
  values: FormikValues,
  formikHelpers: FormikHelpersType,
) => void | Promise<any>

export const getFormikError = (form: FormikType, fieldName: string): string => {
  return get(form.status, [API_ERRORS_KEY, fieldName], '')
}

export const clearFormikErrors = (formikHelpers: FormikHelpersType) => {
  formikHelpers.setStatus({ [API_ERRORS_KEY]: {} })
}

export const formRequest = (
  formikHelpers: FormikHelpersType,
  request: Promise<any>,
) => {
  clearFormikErrors(formikHelpers)
  return new Promise<any>((resolve, reject) => {
    request
      .then((r) => {
        formikHelpers.setSubmitting(false)
        resolve(r)
      })
      .catch((r: HttpResponseError) => {
        formikHelpers.setSubmitting(false)
        reject(r)
      })
  })
}

export const pickFormValuesFromSchema = (
  values: FormikValues,
  validationSchema: ValidationSchemeType,
): FormikValues => {
  const keys = Object.keys(validationSchema.fields)
  return pickBy(values, (value, key) => keys.includes(key))
}
