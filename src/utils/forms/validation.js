import * as Yup from 'yup'
import isString from 'lodash/isString'

export const VALIDATION_TEXTS = {}

export const FormValidators = {}

export const createValidationSchema = (params: Record<string, any>) => {
  return Yup.object().shape(params || {})
}

export const getValidationErrorText = (
  value: string | { key: string, options: Record<string, any> },
) => {
  let text
  if (value?.key) {
    text = VALIDATION_TEXTS[value?.key]
    if (text && value?.options) {
      Object.keys(value?.options).forEach((k) => {
        text = text?.replace(`{{${k}}}`, value?.options[k])
      })
    }
  } else {
    text = VALIDATION_TEXTS[value] || (isString(value) ? value : '')
  }

  return text || 'Неизвестная ошибка'
}

export default Yup
