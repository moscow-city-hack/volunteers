import { useEffect, useRef, useState, useCallback } from 'react'
import { useQuery } from 'react-query'
import size from 'lodash/size'
import type { FetchConfigType } from 'network/types'

/**
 * A custom useEffect hook that only triggers on updates, not on initial mount
 * @param {Function} effect
 * @param {Array<any>} dependencies
 */
export function useUpdateEffect(effect: () => void, dependencies: Array<any> = []) {
  const isInitialMount = useRef(true)

  /* eslint-disable react-hooks/exhaustive-deps */
  useEffect(() => {
    if (isInitialMount.current) {
      isInitialMount.current = false
    } else {
      effect()
    }
  }, dependencies)
}

export const useSliderProgress = ({
  onSlideChange,
  initialSlide,
}: {
  onSlideChange?: (newIndex: number, data: SwiperClass) => void,
  initialSlide?: number,
} = {}) => {
  const [index, setIndex] = useState(initialSlide || 0)

  const _onSlideChange = useCallback(
    (data) => {
      setIndex(data.activeIndex)
      onSlideChange?.(data.activeIndex, data)
    },
    [onSlideChange],
  )

  return { progress: index, onSlideChange: _onSlideChange }
}

export const useFetch = (
  request: (params?: Record<string, any>) => void,
  name: string,
  config: FetchConfigType = {},
) => {
  const { params, queryOptions, dependencies } = config

  return useQuery(
    size(dependencies) > 0 ? [name, ...dependencies] : name,
    () => request(params),
    queryOptions,
  )
}
