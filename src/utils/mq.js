import isPlainObject from 'lodash/isPlainObject'

export type BreakPoints = Record<
  number,
  {
    slidesPerView?: number,
    spaceBetween?: number,
  }
>

export const sliderBreakPoints = (breakpoints: BreakPoints): BreakPoints => {
  const result = {}

  Object.keys(breakpoints).forEach((width: string) => {
    const _width = +width
    result[_width] = isPlainObject(breakpoints[width])
      ? breakpoints[width]
      : { slidesPerView: breakpoints[_width], spaceBetween: 30 }
  })

  return result
}
